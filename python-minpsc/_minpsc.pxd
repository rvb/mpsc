from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp cimport bool

ctypedef unsigned int wval


cdef extern from "src/bounds.cpp" namespace "symconnect":
    pass

cdef extern from "src/cplex_connect.cpp" namespace "symconnect":
    pass

cdef extern from "src/dp_connect.cpp" namespace "symconnect":
    pass

cdef extern from "src/graph.cpp" namespace "symconnect":
    pass

cdef extern from "src/helpers.cpp" namespace "symconnect":
    pass

cdef extern from "src/slow_connect.cpp" namespace "symconnect":
    pass

cdef extern from "src/bounds.h" namespace "symconnect":
    cdef int GetNumObligatoryComponents(const string& path, const string& lower_bounds_type) except +
    cdef vector[vector[size_t]] GetObligatoryComponents(const string& path,
                                                        const string& lower_bounds_type) except +

cdef extern from "src/cplex_connect.h" namespace "symconnect":
    cdef cppclass CplexEx1Solver:
        CplexEx1Solver(size_t start_vertex, const string& lower_bounds_type) except +
        size_t GetEdgesNum() except +
        size_t GetVerticesNum() except +
        bool SetGraph(const string& file_name) except +
        void Transform(bool optimize) except +
        wval GetSolutionUpperBound() except +
        wval Solve() except +
        bool IsAvailable() except +

    cdef cppclass CplexEx2Solver:
        CplexEx2Solver(const string& lower_bounds_type) except +
        size_t GetEdgesNum() except +
        size_t GetVerticesNum() except +
        bool SetGraph(const string& file_name) except +
        void Transform(bool optimize) except +
        wval GetSolutionUpperBound() except +
        wval Solve() except +
        bool IsAvailable() except +

cdef extern from "src/dp_connect.h" namespace "symconnect":
    cdef cppclass DpSolver:
        DpSolver(const double eps, const size_t seed, const string& lower_bounds_type) except +
        size_t GetEdgesNum() except +
        size_t GetVerticesNum() except +
        bool SetGraph(const string& file_name) except +
        void Transform(bool optimize) except +
        wval GetSolutionUpperBound() except +
        int GetNumObligatoryComponents() except +
        wval Solve() except +
        size_t GetNumOptimalSolutionsFound() except +
        double GetMaxOptimalSolutionsDensity() except +

cdef extern from "src/slow_connect.h" namespace "symconnect":
    cdef cppclass SimpleBruteSolver:
        size_t GetEdgesNum() except +
        size_t GetVerticesNum() except +
        bool SetGraph(const string& file_name) except +
        void Transform(bool optimize) except +
        wval Solve() except +

    cdef cppclass SmartBruteSolver:
        SmartBruteSolver(const string& lower_bounds_type) except +
        size_t GetEdgesNum() except +
        size_t GetVerticesNum() except +
        bool SetGraph(const string& file_name) except +
        void Transform(bool optimize) except +
        wval Solve() except +
