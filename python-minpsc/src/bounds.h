#pragma once

#include "numeric.h"
#include "graph.h"

#include <string>
#include <vector>

namespace symconnect {
    enum class LowerBoundsType {
        None,
        ClosestVertex,
        ClosestVertexAndLeaves,
        AnySolution
    };

    LowerBoundsType GetLowerBoundsType(const std::string& name);
    std::vector<wval> CalcLowerBounds(const Graph& graph, const LowerBoundsType type);
    Graph GetObligatoryGraph(const Graph& graph, const std::vector<wval>& lower_bounds);
    int GetNumObligatoryComponents(const std::string& path, const LowerBoundsType lower_bounds_type);
    int GetNumObligatoryComponents(const std::string& path, const std::string& lower_bounds_type);
    std::vector<std::vector<size_t>> GetObligatoryComponents(const std::string& path,
                                                             const LowerBoundsType lower_bounds_type);
    std::vector<std::vector<size_t>> GetObligatoryComponents(const std::string& path,
                                                             const std::string& lower_bounds_type);

    enum class UpperBoundType {
        FarthestVertex,
        Prim
    };

    wval CalcUpperBound(const Graph& graph,
                        const UpperBoundType type,
                        const std::vector<wval>& lower_bounds = {}); // obligatory to pay
    Graph BuildReducedGraph(const Graph& graph,
                            const wval solution_upper_bound,
                            const std::vector<wval>& lower_bounds,
                            const bool cut_edges_once = false);
}
