#pragma once

#include "bounds.h"
#include "solver.h"
#include "numeric.h"
#include "graph.h"

#include <vector>

namespace symconnect {
    class SimpleBruteSolver : public Solver {
    public:
        SimpleBruteSolver() {}
        void Transform(bool) {}
        wval Solve();
    };

    class SmartBruteSolver : public Solver {
    public:
        SmartBruteSolver(const std::string& lower_bounds_type)
            : _lower_bounds_type(GetLowerBoundsType(lower_bounds_type)) {}
        void Transform(bool optimize);
        wval Solve();

    private:
        const LowerBoundsType _lower_bounds_type;
        std::vector<wval> _lowerbounds;
    };
}
