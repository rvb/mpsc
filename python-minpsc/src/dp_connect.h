#pragma once

#include "solver.h"
#include "numeric.h"
#include "graph.h"

#include <vector>
#include <random>
#include <unordered_map>

namespace symconnect {
    class DpSolver : public Solver {
    public:
        DpSolver(const double eps,
                 const size_t seed,
                 const std::string& lower_bounds_type)
            : _eps(eps)
            , _random_engine(seed)
            , _lower_bounds_type(GetLowerBoundsType(lower_bounds_type))
            , _optimized(false)
            , _num_optimal_solutions_found(0)
            , _max_optimal_solutions_density(0)
        {}

        void Transform(bool optimize) override;

        wval GetSolutionUpperBound() const;
        int GetNumObligatoryComponents() const;

        wval Solve() override;

        size_t GetNumOptimalSolutionsFound() const;
        double GetMaxOptimalSolutionsDensity() const;

    private:
        double _eps;
        std::default_random_engine _random_engine;
        LowerBoundsType _lower_bounds_type;

        bool _optimized;
        std::vector<wval> _lowerbounds;
        std::vector<std::vector<size_t>> _comps;
        wval _solution_upperbound;
        wval _lost_answer;
        wval _num_obligatory_components;
        size_t _num_optimal_solutions_found;
        double _max_optimal_solutions_density;
    };
}
