#pragma once

#include <vector>

namespace symconnect {
    bool NextSubset(std::vector<size_t> & subset, size_t set_size);
}
