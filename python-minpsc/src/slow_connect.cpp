#include "slow_connect.h"

#include "algorithm_exception.h"
#include "bounds.h"
#include "helpers.h"

#include <unordered_map>

using namespace std;

namespace symconnect {
    namespace {
        bool SetNextDegArrangement(vector<size_t>& deg, const Graph& g) {
            size_t v = 0;
            while (v < g.Size() && deg[v] == g.Deg(v)) {
                deg[v] = 1;
                ++v;
            }

            if (v == g.Size()) {
                return false;
            }

            ++deg[v];
            return true;
        }
    } // anonymous

    wval SimpleBruteSolver::Solve() {
        auto& g = _graph;

        size_t n = g.Size();

        if (n <= 1) {
            return 0;
        }

        Graph sg = g;
        for (size_t v = 0; v < n; ++v) {
            sg.SortEdges(v);
        }

        vector<size_t> deg(n, 1);

        bool found_solution = false;
        wval best_solution = 0;

        do {
            Graph cg(n);
            for (size_t v = 0; v < n; ++v) {
                for (size_t q = 0; q < deg[v]; ++q) {
                    const Edge & e = sg.GetEdges(v)[q];
                    size_t u = e.to;
                    wval wu = sg.GetEdges(u)[deg[u] - 1].weight;
                    if (v < u && LessEq(e.weight, wu)) {
                        cg.AddEdge(v, u, wval(0));
                    }
                }
            }

            if (cg.FindComponents().size() == 1) {

                wval curr_solution = 0;

                for (size_t v = 0; v < n; ++v) {
                    const Edge & e = sg.GetEdges(v)[deg[v] - 1];
                    curr_solution += e.weight;
                }

                if (!found_solution || best_solution > curr_solution) {
                    found_solution = true;
                    best_solution = curr_solution;
                }
            }
        } while (SetNextDegArrangement(deg, sg));

        if (!found_solution) {
            throw AlgorithmException("No solution found.");
        }

        return best_solution;
    }

    void SmartBruteSolver::Transform(bool) {
        _lowerbounds = CalcLowerBounds(_graph, _lower_bounds_type);
    }

    wval SmartBruteSolver::Solve() {
        auto& g = _graph;
        auto& lbounds = _lowerbounds;

        size_t n = g.Size();

        if (n <= 1) {
            return 0;
        }

        Graph og = GetObligatoryGraph(g, lbounds);
        vector<vector<size_t>> comps = og.FindComponents();

        // find all components
        vector<size_t> comp_nums(n);
        for (size_t comp_num = 0; comp_num < comps.size(); ++comp_num) {
            for (size_t v : comps[comp_num]) {
                comp_nums[v] = comp_num;
            }
        }
        size_t C = comps.size();

        vector<pair<size_t, Edge>> connect_edges; // all different edges connecting different components by their smallest vertex and an edge itself
        for (size_t v = 0; v < n; ++v) {
            const vector<Edge> & edges = g.GetEdges(v);
            for (size_t q = 0; q < edges.size(); ++q) {
                const Edge & edge = edges[q];
                if (v < edge.to && comp_nums[v] != comp_nums[edge.to]) {
                    connect_edges.push_back({v, edge});
                }
            }
        }

        wval lbounds_sum = wval(0);
        for (wval w : lbounds) {
            lbounds_sum += w;
        }

        vector<size_t> subset(C - 1);
        for (size_t i = 0; i < C - 1; ++i) {
            subset[i] = i + 1;
        }

        wval best_sum = CalcUpperBound(g, UpperBoundType::FarthestVertex, lbounds);

        unordered_map<size_t, wval> changed_costs;

        do {
            Graph sg(C); // graph of components of og
            unordered_map<size_t, wval> cur_changed_costs;

            for (size_t edge_num : subset) {
                size_t v = connect_edges[edge_num - 1].first;
                size_t u = connect_edges[edge_num - 1].second.to;
                wval weight = connect_edges[edge_num - 1].second.weight;
                if (Less(cur_changed_costs[v], weight - lbounds[v])) {
                    cur_changed_costs[v] = weight - lbounds[v];
                }
                if (Less(cur_changed_costs[u], weight - lbounds[u])) {
                    cur_changed_costs[u] = weight - lbounds[u];
                }
            }
            wval cur_sum = lbounds_sum;
            for (pair<size_t, wval> p : cur_changed_costs) {
                cur_sum += p.second;
            }

            if (Less(cur_sum, best_sum)) {
                for (size_t edge_num : subset) {
                    size_t v = connect_edges[edge_num - 1].first;
                    size_t u = connect_edges[edge_num - 1].second.to;
                    sg.AddEdge(comp_nums[v], comp_nums[u], wval(0));
                }
                if (sg.FindComponents().size() == 1) {
                    best_sum = cur_sum;
                    changed_costs = cur_changed_costs;
                }
            }
        } while (NextSubset(subset, connect_edges.size()));

        return best_sum;
    }
}
