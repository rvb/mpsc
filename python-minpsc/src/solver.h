#pragma once

#include "numeric.h"
#include "graph.h"
#include <string>

namespace symconnect {
    class Solver {
    public:
        bool SetGraph(const std::string& file_name) {
            Graph graph = ReadGraph(file_name);
            _graph = graph;
            if (_graph.FindComponents().size() > 1) {
                return false;
            }
            return true;
        }

        size_t GetEdgesNum() {
            size_t edges_num = 0;
            for (size_t v = 0; v < _graph.Size(); ++v) {
                edges_num += _graph.GetEdges(v).size();
            }
            edges_num /= 2;
            return edges_num;
        }

        size_t GetVerticesNum() {
            return _graph.Size();
        }

        virtual void Transform(bool optimize) = 0;
        virtual wval Solve() = 0;

    protected:
        Graph _graph;
    };
}
