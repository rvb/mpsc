#pragma once

#include <string>

namespace symconnect {
    class AlgorithmException {
    public:
        AlgorithmException(const std::string& message)
            : message(message) {}

        std::string GetMessage() {
            return message;
        }

    private:
        std::string message;
    };
}
