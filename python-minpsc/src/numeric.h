#ifndef NUMERIC_H
#define NUMERIC_H

using wval = int; // type for edge weights

const double EPS = 1e-12;
const long long INF = 0x3f3f3f3f3f3f3f3fLL;

template<typename T>
void RelaxMin(T & old_value, T new_value) {
    if (new_value < old_value)
        old_value = new_value;
}

template<typename T>
void RelaxMax(T & old_value, const T & new_value) {
    if (old_value < new_value)
        old_value = new_value;
}

template<typename T>
inline int Sign(const T x) {
    if (x < -EPS) return -1;
    if (x > +EPS) return +1;
    return 0;
}

template<typename T>
inline bool Equal(T x, T y) {
    return Sign(x - y) == 0;
}

template<typename T>
inline bool Less(const T x, const T y) {
    return Sign(x - y) < 0;
}

template<typename T>
inline bool LessEq(const T x, const T y) {
    return Sign(x - y) <= 0;
}

template<typename T>
inline bool Greater(const T x, const T y) {
    return Sign(x - y) > 0;
}

template<typename T>
inline bool GreaterEq(const T x, const T y) {
    return Sign(x - y) >= 0;
}

// long long specialization
template<>
inline int Sign<long long>(const long long x) {
    if (x < 0) return -1;
    if (x > 0) return +1;
    return 0;
}

template<>
inline bool Equal<long long>(const long long x, const long long y) {
    return x == y;
}

template<>
inline bool LessEq<long long>(const long long x, const long long y) {
    return x <= y;
}

template<>
inline bool GreaterEq<long long>(const long long x, const long long y) {
    return x >= y;
}

template<>
inline bool Greater<long long>(const long long x, const long long y) {
    return x > y;
}

template<>
inline bool Less<long long>(const long long x, const long long y) {
    return x < y;
}
//

// int specialization
template<>
inline int Sign<int>(const int x) {
    if (x < 0) return -1;
    if (x > 0) return +1;
    return 0;
}

template<>
inline bool Equal<int>(const int x, const int y) {
    return x == y;
}

template<>
inline bool LessEq<int>(const int x, const int y) {
    return x <= y;
}

template<>
inline bool GreaterEq<int>(const int x, const int y) {
    return x >= y;
}

template<>
inline bool Greater<int>(const int x, const int y) {
    return x > y;
}

template<>
inline bool Less<int>(const int x, const int y) {
    return x < y;
}
//

#endif
