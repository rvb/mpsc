#include "graph.h"

#include <algorithm>
#include <fstream>
#include <iostream>

using namespace std;

namespace symconnect {
    Graph::Graph(size_t size) {
        this->size = size;
        edge_lists.resize(size);
    }

    void Graph::AddEdge(size_t u, size_t v, wval w) {
        edge_lists[u].emplace_back(v, w);
        if (u != v) {
            edge_lists[v].emplace_back(u, w);
        }
    }

    void Graph::AddOrientedEdge(size_t u, size_t v, wval w) {
        edge_lists[u].emplace_back(v, w);
    }

    const vector<Edge>& Graph::GetEdges(size_t v) const {
        return edge_lists[v];
    }

    size_t Graph::Size() const {
        return size;
    }

    size_t Graph::Deg(size_t v) const {
        return edge_lists[v].size();
    }

    void Graph::SortEdges(size_t v) {
        sort(edge_lists[v].begin(), edge_lists[v].end(),
            [](const Edge& e1, const Edge& e2)
                {return e1.weight < e2.weight;}
        );
    }

    Graph& Graph::operator=(const Graph& g) {
        this->size = g.size;
        this->edge_lists = g.edge_lists;
        return *this;
    }

    vector<vector<size_t>> Graph::FindComponents() const {
        vector<vector<size_t>> comps;
        vector<bool> marked(size, false);
        for (size_t v = 0; v < size; ++v) {
            if (!marked[v]) {
                comps.push_back(vector<size_t>());
                FindComponentAndMark_BFS(v, comps.back(), marked);
            }
        }
        return comps;
    }

    vector<bool> Graph::GetReachableMask(size_t v) const {
        vector<bool> mask(size);
        vector<size_t> tmp;
        FindComponentAndMark_BFS(v, tmp, mask);
        return mask;
    }

    void Graph::FindComponentAndMark_DFS(
        size_t v,
        vector<size_t>& comp,
        vector<bool>& marked
    ) const {
        marked[v] = true;
        comp.push_back(v);
        for (const Edge& e : edge_lists[v]) {
            if (!marked[e.to]) {
                FindComponentAndMark_DFS(e.to, comp, marked);
            }
        }
    }

    void Graph::FindComponentAndMark_BFS(
        size_t v,
        vector<size_t>& comp,
        vector<bool>& marked
    ) const {
        comp.push_back(v);
        vector<size_t> q(1, v);
        marked[v] = true;
        size_t l = 0;
        while (l != q.size()) {
            size_t u = q[l++];
            for (const Edge& e : edge_lists[u]) {
                if (!marked[e.to]) {
                    marked[e.to] = true;
                    comp.push_back(e.to);
                    q.push_back(e.to);
                }
            }
        }
    }

    Graph ReadGraph(const string& file_name) {
        ifstream input(file_name);

        size_t gsize, edges_num;
        input >> gsize >> edges_num;
        Graph g(gsize);

        size_t u, v;
        wval w;

        for (size_t edge_id = 0; edge_id < edges_num; ++edge_id) {
            input >> u >> v >> w;
            g.AddEdge(u, v, w);
        }

        input.close();

        return g;
    }

    void OutputGraph(const Graph& g, const string& file_name) {
        ofstream output(file_name);

        output << g.Size();
        for (size_t v = 0; v < g.Size(); ++v) {
            for (const Edge& e : g.GetEdges(v)) {
                if (v < e.to) {
                    output << '\n' << v << ' ' << e.to << ' ' << e.weight;
                }
            }
        }

        output.close();
    }
}
