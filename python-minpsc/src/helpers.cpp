#include "helpers.h"

#include <vector>

namespace symconnect {
    bool NextSubset(std::vector<size_t> & subset, size_t set_size) {
        size_t k = subset.size();
        while (k > 0 && subset[k - 1] == set_size - (subset.size() - k)) {
            --k;
        }
        if (k == 0) {
            return false;
        }
        subset[k - 1]++;
        for (; k < subset.size(); ++k) {
            subset[k] = subset[k - 1] + 1;
        }
        return true;
    }
}
