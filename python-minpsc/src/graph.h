#pragma once

#include "numeric.h"

#include <vector>
#include <string>

namespace symconnect {
    struct Edge {
        size_t to;
        wval weight;

        Edge() = default;

        Edge(size_t to, wval weight)
            : to(to), weight(weight) {}
    };

    class Graph {
    public:
        Graph() : Graph(0) {}

        Graph(size_t size);

        void AddEdge(size_t u, size_t v, wval w);

        void AddOrientedEdge(size_t u, size_t v, wval w);

        const std::vector<Edge>& GetEdges(size_t v) const;

        size_t Size() const;

        size_t Deg(size_t v) const;

        void SortEdges(size_t v);

        Graph& operator=(const Graph& g);

        std::vector<std::vector<size_t>> FindComponents() const;

        std::vector<bool> GetReachableMask(size_t v) const;

    private:
        size_t size;
        std::vector<std::vector<Edge>> edge_lists;

        void FindComponentAndMark_DFS(
            size_t v,
            std::vector<size_t>& comp,
            std::vector<bool>& marked
        ) const;

        void FindComponentAndMark_BFS(
            size_t v,
            std::vector<size_t>& comp,
            std::vector<bool>& marked
        ) const;
    };

    Graph ReadGraph(const std::string& file_name);

    void OutputGraph(const Graph& g, const std::string& file_name);
}
