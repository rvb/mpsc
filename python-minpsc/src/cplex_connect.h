#pragma once

#include "bounds.h"
#include "graph.h"
#include "numeric.h"
#include "solver.h"

#include <string>
#include <unordered_map>

#ifdef USE_CPLEX
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wignored-attributes"

#define IL_STD
#include <ilcplex/ilocplex.h>

#pragma GCC diagnostic pop
#endif


namespace symconnect {
    void ToCplexEx1(const Graph& g, const std::string output_file, size_t s, bool optimize);

#ifndef USE_CPLEX
    class CplexSolver : public Solver {
    public:
        CplexSolver(const std::string& lower_bounds_type) {}
        wval GetSolutionUpperBound() const;
        LowerBoundsType GetLowerBoundsType() const;
        inline bool IsAvailable() const { return false; }
    };
#else
    class CplexSolver : public Solver {
    public:
        CplexSolver(const std::string& lower_bounds_type)
            : _env()
            , _model(_env)
            , _lower_bounds_type(symconnect::GetLowerBoundsType(lower_bounds_type))
        {}

        wval GetSolutionUpperBound() const;
        LowerBoundsType GetLowerBoundsType() const;
        inline bool IsAvailable() const { return true; }

    protected:
        IloEnv _env;
        IloModel _model;
        wval _solution_upperbound;

    private:
        LowerBoundsType _lower_bounds_type;
    };
#endif

    class CplexEx1Solver : public CplexSolver {
    public:
        CplexEx1Solver(size_t start_vertex,
                       const std::string& lower_bounds_type)
            : CplexSolver(lower_bounds_type)
            , _start_vertex(start_vertex)
        {
        }

        void Transform(bool optimize);

        wval Solve();

    private:
        size_t _start_vertex;
    };

    class CplexEx2Solver : public CplexSolver {
    public:
        CplexEx2Solver(const std::string& lower_bounds_type)
            : CplexSolver(lower_bounds_type)
        {
        }

        void Transform(bool optimize);

        wval Solve();
#ifdef USE_CPLEX
    private:
        std::unordered_map<size_t, IloNumVar> _y_mapping;
        std::unordered_map<size_t, IloNumVar> _z_mapping;
#endif
    };
}
