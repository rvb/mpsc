#include "cplex_connect.h"

#include "bounds.h"

#include <unordered_map>
#include <vector>

using namespace std;

namespace symconnect {
#ifndef USE_CPLEX
    wval CplexSolver::GetSolutionUpperBound() const { return 0; }
    LowerBoundsType CplexSolver::GetLowerBoundsType() const { return LowerBoundsType::None; }
    void CplexEx1Solver::Transform(bool optimize) {}
    wval CplexEx1Solver::Solve() { return 0; }
    void CplexEx2Solver::Transform(bool optimize) {}
    wval CplexEx2Solver::Solve() { return 0; }
#else
    static pair<Graph, Graph> BuildLBGraph(const Graph& reduced_graph) {
        Graph graph_a(reduced_graph.Size());
        Graph graph_a_inv(reduced_graph.Size());
        for (size_t v = 0; v < reduced_graph.Size(); ++v) {
            const Edge& e = reduced_graph.GetEdges(v)[0];
            graph_a.AddOrientedEdge(v, e.to, wval(0));
            graph_a_inv.AddOrientedEdge(e.to, v, wval(0));
        }
        return {graph_a, graph_a_inv};
    }

    static unordered_map<size_t, IloNumVar> AddObjectiveAndVariablesY(
        IloEnv& env,
        IloModel& model,
        const Graph& rg,
        const std::vector<wval>& lb
    ) {
        unordered_map<size_t, IloNumVar> yMapping;
        size_t n = rg.Size();
        IloExpr obj_expr(env);
        for (size_t v = 0; v < n; ++v) {
            const vector<Edge> & edges = rg.GetEdges(v);
            for (size_t i = 0; i < edges.size(); ++i) {
                const Edge & e = edges[i];
                auto& y = yMapping[v * n + e.to] = IloNumVar(env, 0, 1, ILOBOOL);
                if (LessEq(e.weight, lb[v]) && LessEq(e.weight, lb[e.to])) {
                    model.add(IloRange(env, 1, y, 1));
                }
                wval c = edges[i].weight - (i == 0 ? 0 : edges[i-1].weight);
                obj_expr += c * yMapping[v * n + e.to];
            }
        }
        model.add(IloMinimize(env, obj_expr));
        return yMapping;
    }

    static unordered_map<size_t, IloNumVar> AddVariablesX(IloEnv& env, const Graph& rg) {
        unordered_map<size_t, IloNumVar> xMapping;
        size_t n = rg.Size();
        for (size_t v = 0; v < n; ++v) {
            const vector<Edge> & edges = rg.GetEdges(v);
            for (size_t i = 0; i < edges.size(); ++i) {
                const Edge & e = edges[i];
                xMapping[v * n + e.to] = IloNumVar(env, 0, IloInfinity, ILOFLOAT);
            }
        }
        return xMapping;
    }

    static unordered_map<size_t, IloNumVar> AddVariablesZ(IloEnv& env,
                                                          const Graph& rg,
                                                          const std::vector<wval>& lb) {
        unordered_map<size_t, IloNumVar> zMapping;
        size_t n = rg.Size();
        for (size_t v = 0; v < n; ++v) {
            const vector<Edge> & edges = rg.GetEdges(v);
            for (size_t i = 0; i < edges.size(); ++i) {
                const Edge & e = edges[i];
                if (v < e.to) {
                    zMapping[v * n + e.to] = IloNumVar(env, 0, 1, ILOBOOL);
                }
            }
        }
        return zMapping;
    }

    static void AddEx1Restrictions(
        IloEnv& env,
        IloModel& model,
        const Graph& rg,
        unordered_map<size_t, IloNumVar>& y,
        unordered_map<size_t, IloNumVar>& x,
        size_t s
    ) {
        size_t n = rg.Size();
        for (size_t v = 0; v < n; ++v) {
            const vector<Edge> & edges = rg.GetEdges(v);
            size_t a = v;
            for (size_t i = 0; i < edges.size(); ++i) {
                const Edge & e = edges[i];
                if (i != 0) {
                    model.add(IloRange(env, -IloInfinity, y[v * n + e.to] - y[v * n + a], 0));
                }
                model.add(IloRange(env, -IloInfinity, x[v * n + e.to] - int(n-1) * y[v * n + e.to], 0));
                model.add(IloRange(env, -IloInfinity, x[v * n + e.to] - int(n-1) * y[e.to * n + v], 0));
                a = e.to;
            }
            IloExpr r(env);
            for (size_t i = 0; i < edges.size(); ++i) {
                const Edge & e = edges[i];
                r += x[v * n + e.to] - x[e.to * n + v];
            }
            long long value = (v == s) ? (long long)(n - 1) : -1;
            model.add(IloRange(env, value, r, value));
        }
    }

    static void AddOptimizations18Through20(
        IloEnv& env,
        IloModel& model,
        const Graph& rg,
        unordered_map<size_t, IloNumVar>& y
    ) {
        size_t n = rg.Size();
        for (size_t v = 0; v < n; ++v) {
            const vector<Edge> & edges = rg.GetEdges(v);
            size_t a = v;
            for (size_t i = 0; i < edges.size(); ++i) {
                const Edge & e = edges[i];
                if (i == 0) {
                    model.add(IloRange(env, 1, y[v * n + e.to], 1));
                } else {
                    model.add(IloRange(env, 0, y[a * n + v] - y[v * n + a] + y[v * n + e.to], IloInfinity));
                    if (i+1 == edges.size())
                        model.add(IloRange(env, 0, y[e.to * n + v] - y[v * n + e.to], IloInfinity));
                }
                a = e.to;
            }
        }
    }

    static void AddOptimization23(
        IloEnv& env,
        IloModel& model,
        const Graph& ga,
        const Graph& rg,
        unordered_map<size_t, IloNumVar>& y
    ) {
        size_t n = ga.Size();
        for (size_t v = 0; v < n; ++v) {
            vector<bool> Rv = ga.GetReachableMask(v);
            IloExpr r(env);
            bool empty = true;
            for (size_t k = 0; k < n; ++k) {
                if (!Rv[k])
                    continue;
                const vector<Edge> & edges = rg.GetEdges(k);
                for (const Edge & e : edges) {
                    if (!Rv[e.to]) {
                        r += y[k * n + e.to];
                        empty = false;
                    }
                }
            }
            if (!empty) {
                model.add(IloRange(env, 1, r, IloInfinity));
            }
        }
    }

    static void AddOptimization24(
        IloEnv& env,
        IloModel& model,
        const Graph& gainv,
        const Graph& rg,
        unordered_map<size_t, IloNumVar>& y
    ) {
        size_t n = gainv.Size();
        for (size_t v = 0; v < n; ++v) {
            vector<bool> Qv = gainv.GetReachableMask(v);
            IloExpr r(env);
            bool empty = true;
            for (size_t k = 0; k < n; ++k) {
                if (!Qv[k])
                    continue;
                const vector<Edge> & edges = rg.GetEdges(k);
                for (const Edge & e : edges) {
                    if (!Qv[e.to]) {
                        r += y[k * n + e.to];
                        empty = false;
                    }
                }
            }
            if (!empty) {
                model.add(IloRange(env, 1, r, IloInfinity));
            }
        }
    }

    static void AddOptimization25(
        IloEnv& env,
        IloModel& model,
        const Graph& rg,
        unordered_map<size_t, IloNumVar>& y
    ) {
        size_t n = rg.Size();
        IloExpr r(env);
        for (size_t v = 0; v < n; ++v) {
            const vector<Edge> & edges = rg.GetEdges(v);
            for (size_t i = 1; i < edges.size(); ++i) {
                const Edge & e = edges[i];
                r += y[v * n + e.to];
            }
        }
        model.add(IloRange(env, n - 2, r, IloInfinity));
    }

    static void AddAllOptimizations(
        IloEnv& env,
        IloModel& model,
        const Graph& rg,
        const Graph& ga,
        const Graph& gainv,
        unordered_map<size_t, IloNumVar>& yMapping
    ) {
        AddOptimizations18Through20(env, model, rg, yMapping);
        AddOptimization23(env, model, ga, rg, yMapping);
        AddOptimization24(env, model, gainv, rg, yMapping);
        AddOptimization25(env, model, rg, yMapping);
    }

    wval CplexSolver::GetSolutionUpperBound() const {
        return _solution_upperbound;
    }

    LowerBoundsType CplexSolver::GetLowerBoundsType() const {
        return _lower_bounds_type;
    }

    void CplexEx1Solver::Transform(bool optimize) {
        _solution_upperbound = CalcUpperBound(_graph, UpperBoundType::Prim);
        auto lb = CalcLowerBounds(_graph, GetLowerBoundsType());
        _graph = BuildReducedGraph(_graph, _solution_upperbound, lb);
        for (size_t v = 0; v < _graph.Size(); ++v) {
            _graph.SortEdges(v);
        }

        auto lb_graphs = BuildLBGraph(_graph); // for opt 23, 24
        const auto& graph_a = lb_graphs.first;
        const auto& graph_a_inv = lb_graphs.second;

        auto y_mapping = AddObjectiveAndVariablesY(_env, _model, _graph, lb);
        AddAllOptimizations(_env, _model, _graph, graph_a, graph_a_inv, y_mapping);

        auto x_mapping = AddVariablesX(_env, _graph);
        AddEx1Restrictions(_env, _model, _graph, y_mapping, x_mapping, _start_vertex);
    }

    wval CplexEx1Solver::Solve() {
        IloCplex cplex(_env);
        cplex.setOut(_env.getNullStream());

        cplex.extract(_model);
        cplex.solve();

        wval result = cplex.getObjValue();
        _env.end();
        return result;
    }

    static void AddEx2BaseRestrictions(
        IloEnv& env,
        IloModel& model,
        const Graph& rg,
        unordered_map<size_t, IloNumVar>& y,
        unordered_map<size_t, IloNumVar>& z
    ) {
        size_t n = rg.Size();
        IloExpr init_sum(env);
        for (size_t v = 0; v < n; ++v) {
            const vector<Edge> & edges = rg.GetEdges(v);
            size_t a = v;
            for (size_t i = 0; i < edges.size(); ++i) {
                const Edge & e = edges[i];
                if (i != 0) {
                    model.add(IloRange(env, -IloInfinity, y[v * n + e.to] - y[v * n + a], 0));
                }
                if (v < e.to) {
                    model.add(IloRange(env, 0, y[v * n + e.to] - z[v * n + e.to], IloInfinity));
                    model.add(IloRange(env, 0, y[e.to * n + v] - z[v * n + e.to], IloInfinity));
                    init_sum += z[v * n + e.to];
                }
                a = e.to;
            }
        }
        model.add(IloRange(env, n - 1, init_sum, IloInfinity));
    }

    static vector<vector<size_t>> FindIsolatedComponents(
        const Graph& rg,
        const IloCplex& cplex,
        unordered_map<size_t, IloNumVar>& y
    ) {
        size_t n = rg.Size();
        Graph g(n);
        for (size_t v = 0; v < n; ++v) {
            for (const auto& e : rg.GetEdges(v)) {
                if (v < e.to && cplex.getValue(y[v * n + e.to]) && cplex.getValue(y[e.to * n + v])) {
                    g.AddEdge(v, e.to, 0);
                }
            }
        }
        return g.FindComponents();
    }

    void AddNewEx2Restriction(
        IloEnv& env,
        IloModel& model,
        const Graph& rg,
        const vector<vector<size_t>>& components,
        unordered_map<size_t, IloNumVar>& z
    ) {
        size_t n = rg.Size();
        vector<size_t> comp_nums(n);
        for (size_t comp_num = 0; comp_num < components.size(); ++comp_num) {
            for (size_t v : components[comp_num]) {
                comp_nums[v] = comp_num;
            }
        }
        for (size_t comp_num = 0; comp_num < components.size(); ++comp_num) {
            IloExpr out_sum(env);
            for (size_t v : components[comp_num]) {
                for (const auto& e : rg.GetEdges(v)) {
                    if (comp_nums[e.to] == comp_num) {
                        continue;
                    }
                    size_t z_index = min(v, e.to) * n + max(v, e.to);
                    out_sum += z[z_index];
                }
            }
            model.add(IloRange(env, 1, out_sum, IloInfinity));
        }
    }

    void CplexEx2Solver::Transform(bool optimize) {
        _solution_upperbound = CalcUpperBound(_graph, UpperBoundType::Prim);
        auto lb = CalcLowerBounds(_graph, GetLowerBoundsType());
        _graph = BuildReducedGraph(_graph, _solution_upperbound, lb);
        for (size_t v = 0; v < _graph.Size(); ++v) {
            _graph.SortEdges(v);
        }

        auto lb_graphs = BuildLBGraph(_graph); // for opt 23, 24
        const auto& graph_a = lb_graphs.first;
        const auto& graph_a_inv = lb_graphs.second;

        _y_mapping = AddObjectiveAndVariablesY(_env, _model, _graph, lb);
        AddAllOptimizations(_env, _model, _graph, graph_a, graph_a_inv, _y_mapping);

        _z_mapping = AddVariablesZ(_env, _graph, lb);
        AddEx2BaseRestrictions(_env, _model, _graph, _y_mapping, _z_mapping);
    }

    wval CplexEx2Solver::Solve() {
        IloCplex cplex(_env);
        cplex.setOut(_env.getNullStream());

        for (size_t iterations = 0;; ++iterations) {
            cplex.extract(_model);
            cplex.solve();

            vector<vector<size_t>> components = FindIsolatedComponents(_graph, cplex, _y_mapping);
            if (components.size() == 1) {
                cout << iterations << " restrictions were added" << endl;
                break;
            }
            AddNewEx2Restriction(_env, _model, _graph, components, _z_mapping);
        }

        wval result = cplex.getObjValue();

        _env.end();
        return result;
    }
#endif
}
