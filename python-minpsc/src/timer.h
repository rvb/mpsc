#pragma once

#include <ctime>

namespace symconnect {
    class Timer {
    public:
        Timer() {
            Zero();
        }

        double Check() const {
            return (double(clock()) - zero_time) / CLOCKS_PER_SEC;
        }

        void Zero() {
            zero_time = clock();
        }

    private:
        clock_t zero_time;
    };
}
