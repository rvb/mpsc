#include "dp_connect.h"

#include "algorithm_exception.h"
#include "bounds.h"
#include "helpers.h"
#include "timer.h"

#include <iostream>

using namespace std;

namespace symconnect {
    using ColorSet = long long;
    const int MAX_SET_SIZE = 64;

    namespace {
        struct DpState {
            ColorSet color_set;
            size_t v;
            size_t q_ind;

            DpState(const DpState & s)
                : color_set(s.color_set), v(s.v), q_ind(s.q_ind) {}
            DpState(ColorSet color_set, size_t v, size_t q_ind)
                : color_set(color_set), v(v), q_ind(q_ind) {}
        };

        struct MemoryForDp {
            wval* Dval;
            size_t* suffD;
            size_t* binsearch_precalc;
        };

        MemoryForDp AllocateMemoryForDp(size_t max_C, size_t max_pg_size) {
            size_t base1 = max_pg_size;
            size_t base2 = base1 * max_pg_size;
            size_t base3 = base2 * (1LL << max_C);

            MemoryForDp memory;

            memory.Dval = (wval*)malloc(sizeof(wval) * base3);
            if (!memory.Dval) {
                throw(std::bad_alloc());
            }

            memory.suffD = (size_t*)malloc(sizeof(size_t) * base3);
            if (!memory.suffD) {
                throw(std::bad_alloc());
            }

            memory.binsearch_precalc = (size_t*)malloc(sizeof(size_t) * max_pg_size * max_pg_size);
            if (!memory.binsearch_precalc) {
                throw(std::bad_alloc());
            }

            return memory;
        }

        void DeallocateMemoryForDp(MemoryForDp& memory) {
            free(memory.Dval);
            free(memory.suffD);
            free(memory.binsearch_precalc);
        }

        bool CalcDp(
            const Graph& pg,
            const wval* lbounds,
            const size_t* col,
            size_t C,
            const vector<size_t> & comp_by_col,
            const vector<ColorSet> colset_by_comp,
            wval solution_ubound,
            bool has_isolated_vertices,
            wval& answer,
            MemoryForDp& memory
        ) {
            const wval inf(solution_ubound + 1);

            size_t base1 = pg.Size();
            size_t base2 = base1 * pg.Size();
            size_t base3 = base2 * (1LL << C);

            vector<bool> Dok(base3);
            wval* Dval = memory.Dval;
            size_t* suffD = memory.suffD;
            size_t* binsearch_precalc = memory.binsearch_precalc;

            DpState* bestdp_for_colset = (DpState*)malloc(sizeof(DpState) * (1LL << C));
            if (!bestdp_for_colset) throw(std::bad_alloc());

            for (size_t v = 0; v < pg.Size(); ++v) {
                const vector<Edge> & Nv = pg.GetEdges(v);
                for (size_t u_ind = 0; u_ind < Nv.size(); ++u_ind) {
                    size_t u = Nv[u_ind].to;
                    const vector<Edge> & Nu = pg.GetEdges(u);
                    wval w_uv = Nv[u_ind].weight;

                    // search where should q1_ind go to
                    size_t l = 0, r = Nu.size() + 1;
                    while (r - l > 1) {
                        size_t m = ((r + l) >> 1);
                        if (GreaterEq(Nu[m - 1].weight, w_uv))
                            r = m;
                        else
                            l = m;
                    }
                    binsearch_precalc[pg.Size() * v + u_ind] = r - 1;
                }
            }

            for (size_t color_set_size = 0; color_set_size <= C; ++color_set_size) {
                vector<ColorSet> color_sets;
                for (ColorSet color_set = (ColorSet(1) << color_set_size) - 1; color_set <= (ColorSet(1) << C) - 1;) {
                    color_sets.push_back(color_set);
                    if (color_set == 0) {
                        break;
                    }
                    ColorSet x = color_set & -color_set;
                    ColorSet y = color_set + x;
                    color_set = (((color_set & ~y) / x) >> 1) | y;
                }
                for (size_t color_set_id = 0; color_set_id < color_sets.size(); ++color_set_id) {
                    ColorSet color_set = color_sets[color_set_id];
                    bestdp_for_colset[color_set] = DpState(color_set, 0, 0);

                    for (size_t v = 0; v < pg.Size(); ++v) {
                        const vector<Edge> & Nv = pg.GetEdges(v);
                        size_t common_index_part_D_color_set_v = base2 * color_set + base1 * v;

                        for (size_t q_ind = 0; q_ind < Nv.size(); ++q_ind) {
                            suffD[common_index_part_D_color_set_v + q_ind] = q_ind;
                            Dval[common_index_part_D_color_set_v + q_ind] = inf;
                            Dok[common_index_part_D_color_set_v + q_ind] = false;
                        }

                        if (!(color_set & (ColorSet(1) << col[v])))
                            continue;

                        size_t cur_comp = comp_by_col[col[v]];

                        for (ssize_t q_ind = Nv.size() - 1; q_ind >= 0; --q_ind) {
                            if (!(q_ind + 1 < ssize_t(Nv.size()) && Equal(Nv[q_ind + 1].weight, Nv[q_ind].weight))) {
                                wval cost = max(wval(0), Nv[q_ind].weight - lbounds[v]);
                                size_t index = common_index_part_D_color_set_v + q_ind;

                                if (color_set == (ColorSet(1) << col[v])) {
                                    Dval[index] = cost;
                                    Dok[index] = true;
                                } else {
                                    // first dp-transition type
                                    ColorSet mask = color_set ^ (ColorSet(1) << col[v]);
                                    // searching through all submasks of mask
                                    for (ColorSet submask1 = mask & (mask - 1); submask1 != 0; submask1 = (submask1 - 1) & mask) {
                                        ColorSet submask2 = mask ^ submask1;
                                        wval dp1val = Dval[base2 * (submask1 ^ (ColorSet(1) << col[v])) + base1 * v + q_ind];
                                        bool dp1ok = Dok[base2 * (submask1 ^ (ColorSet(1) << col[v])) + base1 * v + q_ind];
                                        wval dp2val = Dval[base2 * (submask2 ^ (ColorSet(1) << col[v])) + base1 * v + q_ind];
                                        bool dp2ok = Dok[base2 * (submask2 ^ (ColorSet(1) << col[v])) + base1 * v + q_ind];

                                        if (dp1ok && dp2ok) {
                                            wval newvalue = dp1val + dp2val - cost;
                                            if (Dval[index] > newvalue) {
                                                Dval[index] = newvalue;
                                                Dok[index] = true;
                                            }
                                        }
                                    }

                                    // second dp-transition type
                                    ColorSet same_comp_colors = (color_set & colset_by_comp[cur_comp]) ^ (ColorSet(1) << col[v]);
                                    if (same_comp_colors != ColorSet(0)) {
                                        size_t col_num = 0;
                                        while (((same_comp_colors >> col_num) & ColorSet(1)) == 0)
                                            ++col_num;
                                        DpState & dps = bestdp_for_colset[color_set ^ (ColorSet(1) << col[v])];
                                        wval dpval = Dval[base2 * dps.color_set + base1 * dps.v + dps.q_ind];
                                        bool dpok = Dok[base2 * dps.color_set + base1 * dps.v + dps.q_ind];

                                        if (dpok) {
                                            if (Dval[index] > dpval + cost) {
                                                Dval[index] = dpval + cost;
                                                Dok[index] = true;
                                            }
                                        }
                                    } else {
                                        wval w_vq = Nv[q_ind].weight;
                                        for (size_t u_ind = 0; u_ind < Nv.size() && GreaterEq(w_vq, Nv[u_ind].weight); ++u_ind) {
                                            size_t u = Nv[u_ind].to;

                                            if (((ColorSet(1) << col[u]) & (color_set ^ (ColorSet(1) << col[v]))) == ColorSet(0))
                                                continue;

                                            size_t downto = binsearch_precalc[pg.Size() * v + u_ind];
                                            size_t common_index_part = base2 * (color_set ^ (ColorSet(1) << col[v])) + base1 * u;

                                            size_t mindp_u_ind = suffD[common_index_part + downto];
                                            wval mindpval = Dval[common_index_part + mindp_u_ind];
                                            bool mindpok = Dok[common_index_part + mindp_u_ind];
                                            if (mindpok) {
                                                wval newvalue = mindpval + cost;
                                                if (Dval[index] > newvalue) {
                                                    Dval[index] = newvalue;
                                                    Dok[index] = true; // TODO: check if this shouldn't be outside the scope
                                                }
                                            }

                                        }
                                    }
                                }
                                // relax bestdp_for_colset
                                {
                                    DpState & dps = bestdp_for_colset[color_set];
                                    wval dpval = Dval[base2 * dps.color_set + base1 * dps.v + dps.q_ind];
                                    if (Dok[index] && Dval[index] < dpval)
                                        dps = DpState(color_set, v, q_ind);
                                }
                            }

                            // relax suffD
                            size_t & suff_best_q_ind = suffD[common_index_part_D_color_set_v + q_ind];
                            //suff_best_q_ind = q_ind;
                            if (q_ind != ssize_t(Nv.size()) - 1) {
                                size_t suff_best_q_ind_rival = suffD[common_index_part_D_color_set_v + q_ind + 1];
                                if (Dok[common_index_part_D_color_set_v + suff_best_q_ind_rival]) {
                                    if (!Dok[common_index_part_D_color_set_v + suff_best_q_ind]
                                        || Greater(Dval[common_index_part_D_color_set_v + suff_best_q_ind],
                                                   Dval[common_index_part_D_color_set_v + suff_best_q_ind_rival])) {
                                        suff_best_q_ind = suff_best_q_ind_rival;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            DpState& best_dps = bestdp_for_colset[(ColorSet(1) << C) - 1];
            answer = Dval[base2 * best_dps.color_set + base1 * best_dps.v + best_dps.q_ind];

            for (size_t v = 0; v < pg.Size(); ++v) {
                answer += lbounds[v];
            }

            bool good = Dok[base2 * best_dps.color_set + base1 * best_dps.v + best_dps.q_ind];

            free(bestdp_for_colset);

            return good;
        }

        bool RunDp(
            const Graph & pg,
            const vector<wval> & lbounds,
            const vector<size_t> & col,
            size_t C,
            const vector<size_t> & comp_by_col,
            const vector<ColorSet> & colset_by_comp,
            wval solution_ubound,
            bool has_isolated_vertices,
            wval& solution,
            MemoryForDp& memory
        ) {

            if (C - 1 >= MAX_SET_SIZE)
                throw AlgorithmException(string() + "Cannot create color set with " + std::to_string(C - 1) + "indice");

            // calc DP
            return CalcDp(
                pg,
                &lbounds[0],
                &col[0],
                C,
                comp_by_col,
                colset_by_comp,
                solution_ubound,
                has_isolated_vertices,
                solution,
                memory
            );
        }

        size_t CalcAlgoRunTimes(const vector<size_t> & ci, double eps) {
            double num = log(eps);
            double prod = 1.0;
            for (size_t i = 0; i < ci.size(); ++i) {
                for (size_t j = 1; j <= ci[i]; ++j)
                    prod *= double(j) / double(ci[i]);
            }
            double den = log(1.0 - prod);
            double res = num / den;
            return (GreaterEq<double>(res, INF) ? INF : max(size_t(1), size_t(res + EPS)));
        }
    }

    void DpSolver::Transform(bool optimize) {
        // calc lower bounds (and upperbounds if needed)
        _lowerbounds = CalcLowerBounds(_graph, _lower_bounds_type);
        if (optimize) {
            _solution_upperbound = CalcUpperBound(_graph, UpperBoundType::Prim);
            _graph = BuildReducedGraph(_graph, _solution_upperbound, _lowerbounds);
        }

        // find obligatory subgraph
        Graph obligatory_graph = GetObligatoryGraph(_graph, _lowerbounds);

        // find all components
        _comps = obligatory_graph.FindComponents();
        vector<size_t> comp_nums(_graph.Size());
        for (size_t comp_num = 0; comp_num < _comps.size(); ++comp_num) {
            for (size_t v : _comps[comp_num]) {
                comp_nums[v] = comp_num;
            }
        }
        _num_obligatory_components = _comps.size();

        // calc solution cost lower bound
        wval solution_lbound = 0;
        for (wval val : _lowerbounds) {
            solution_lbound += val;
        }

        // build padded graph and sort edges
        Graph padded_graph(_graph.Size());
        for (size_t v = 0; v < _graph.Size(); ++v) {
            for (const Edge & edge : _graph.GetEdges(v)) {
                size_t u = edge.to;
                if (v < u && comp_nums[v] != comp_nums[u]) {
                    if (!optimize ||
                        LessEq(solution_lbound + (edge.weight - _lowerbounds[v])
                                               + (edge.weight - _lowerbounds[u]),
                               _solution_upperbound)) {
                        padded_graph.AddEdge(v, u, edge.weight);
                    }
                }
            }
            padded_graph.AddEdge(v, v, wval(0));
        }

        // build padded graph and sort edges
        // mapping
        _lost_answer = 0;
        if (optimize) {
            vector<size_t> mapping(padded_graph.Size(), padded_graph.Size());
            size_t mapped_graph_size = 0;
            for (size_t v = 0; v < padded_graph.Size(); ++v) {
                if (padded_graph.Deg(v) > 1) {
                    mapping[v] = mapped_graph_size++;
                }
            }
            Graph padded_graph_mapped(mapped_graph_size);
            for (size_t v = 0; v < padded_graph.Size(); ++v) {
                if (mapping[v] != padded_graph.Size()) {
                    for (const Edge& edge : padded_graph.GetEdges(v)) {
                        size_t u = edge.to;
                        if (mapping[v] < mapping[u]) {
                            padded_graph_mapped.AddEdge(mapping[v], mapping[u], edge.weight);
                        }
                    }
                    padded_graph_mapped.AddEdge(mapping[v], mapping[v], wval(0));
                }
            }
            vector<wval> lowerbounds_mapped(padded_graph_mapped.Size());
            for (size_t v = 0; v < padded_graph.Size(); ++v) {
                if (mapping[v] != padded_graph.Size()) {
                    lowerbounds_mapped[mapping[v]] = _lowerbounds[v];
                } else {
                    _lost_answer += _lowerbounds[v];
                }
            }
            vector<vector<size_t>> comps_mapped(_comps.size());
            for (size_t comp_num = 0; comp_num < comps_mapped.size(); ++comp_num) {
                for (size_t v : _comps[comp_num]) {
                    if (mapping[v] != padded_graph.Size()) {
                        comps_mapped[comp_num].push_back(mapping[v]);
                    }
                }
            }

            swap(_comps, comps_mapped);
            swap(_lowerbounds, lowerbounds_mapped);
            _graph = padded_graph_mapped;
        } else {
            _graph = padded_graph;
        }

        // sort graph edges
        for (size_t v = 0; v < _graph.Size(); ++v) {
            _graph.SortEdges(v);
        }

        // calc upper bound if needed
        _solution_upperbound = CalcUpperBound(_graph, UpperBoundType::FarthestVertex, _lowerbounds);
        _optimized = optimize;
    }

    wval DpSolver::GetSolutionUpperBound() const {
        return _solution_upperbound;
    }

    int DpSolver::GetNumObligatoryComponents() const {
        return _num_obligatory_components;
    }

    wval DpSolver::Solve() {
        // run dp for random colorings
        size_t c = _comps.size();
        vector<size_t> subset(c); // this stands for (c1, c1 + c2, ... , c1 + c2 + ... + cc),
                                  // which is a c-size subset of (1, ... , 2 * (c - 1))
        for (size_t i = 0; i < c; ++i)
            subset[i] = i + 1;

        wval best_solution;
        best_solution = _solution_upperbound + 1;

        MemoryForDp memory = AllocateMemoryForDp(2 * c - 2, _graph.Size());

        _num_optimal_solutions_found = 0;
        _max_optimal_solutions_density = 0;
        do {
            // restore ci from q
            vector<size_t> ci(c);
            bool sizes_correct = true;
            for (size_t i = 0; i < c; ++i) {
                ci[i] = subset[i] - ((i > 0) ? subset[i - 1] : 0);
                sizes_correct = sizes_correct && (ci[i] <= _comps[i].size());
            }
            if (!sizes_correct) {
                continue;
            }

            size_t times = CalcAlgoRunTimes(ci, _eps);
            if (times >= INF) {
                throw new AlgorithmException("DP should be run too many times for this eps.");
            }

            size_t num_optimal_solutions_in_round = 0;
            const size_t total_runs = times;
            // repeat dp on random colorings
            while (times--) {
                // color vertices
                vector<ColorSet> colset_by_comp(c);
                vector<size_t> comp_by_col(subset.back());
                vector<size_t> col(_graph.Size());
                for (size_t i = 0; i < c; ++i) {
                    size_t l = (i > 0) ? subset[i - 1] : 0;
                    size_t r = subset[i] - 1;
                    uniform_int_distribution<int> distr(l, r);

                    colset_by_comp[i] = (ColorSet(1) << (r + 1)) - ((ColorSet(1) << (l)));
                    for (size_t col_id = l; col_id <= r; ++col_id) {
                        comp_by_col[col_id] = i;
                    }

                    const vector<size_t> & comp = _comps[i];
                    for (size_t v : comp) {
                        col[v] = distr(_random_engine);
                    }
                }

                // run DP in coloring and relax the answer
                wval curr_solution;
                if (RunDp(
                        _graph,
                        _lowerbounds,
                        col,
                        subset.back(),
                        comp_by_col,
                        colset_by_comp,
                        _solution_upperbound,
                        _optimized,
                        curr_solution,
                        memory
                    )
                ) {
                    if (Less(best_solution, curr_solution)) {
                        continue;
                    }
                    if (Equal(best_solution, curr_solution)) {
                        ++_num_optimal_solutions_found;
                        ++num_optimal_solutions_in_round;
                        continue;
                    }
                    best_solution = curr_solution;
                    _num_optimal_solutions_found = 1;
                    num_optimal_solutions_in_round = 1;
                    _max_optimal_solutions_density = 0;
                }
            }
            RelaxMax(_max_optimal_solutions_density,
                     static_cast<double>(num_optimal_solutions_in_round) /
                     static_cast<double>(total_runs));
        } while (NextSubset(subset, max(c, (c - 1) * 2)));

        DeallocateMemoryForDp(memory);

        best_solution += _lost_answer;
        return best_solution;
    }

    size_t DpSolver::GetNumOptimalSolutionsFound() const {
        return _num_optimal_solutions_found;
    }

    double DpSolver::GetMaxOptimalSolutionsDensity() const {
        return _max_optimal_solutions_density;
    }
}
