#include "bounds.h"

#include <cassert>
#include <iostream>
#include <map>
#include <queue>
#include <stdexcept>
#include <vector>

using namespace std;

namespace symconnect {
    namespace {
        const std::map<std::string, LowerBoundsType> LowerBoundsTypeNames = {
            {"none", LowerBoundsType::None},
            {"closest-vertex", LowerBoundsType::ClosestVertex},
            {"closest-vertex-and-leaves", LowerBoundsType::ClosestVertexAndLeaves},
            {"any-solution", LowerBoundsType::AnySolution}
        };

        void CalcClosestVertexLB(const Graph& graph, std::vector<wval>* lower_bounds) {
            assert(lower_bounds);

            for (size_t v = 0; v < graph.Size(); ++v) {
                if (graph.Deg(v) == 0) {
                    continue;
                }
                const auto& edges = graph.GetEdges(v);
                wval bound = edges[0].weight;
                for (const Edge& e : edges) {
                    RelaxMin(bound, e.weight);
                }
                RelaxMax((*lower_bounds)[v], bound);
            }
        }

        void CalcLeavesLB(const Graph& graph, std::vector<wval>* lower_bounds) {
            assert(lower_bounds);

            for (size_t v = 0; v < graph.Size(); ++v) {
                if (graph.Deg(v) != 1) {
                    continue;
                }
                const Edge& e = graph.GetEdges(v)[0];
                RelaxMax((*lower_bounds)[v], e.weight);
                RelaxMax((*lower_bounds)[e.to], e.weight);
            }
        }

        void CalcAnySolutionLB(const Graph& graph, std::vector<wval>* lower_bounds) {
            assert(lower_bounds);

            for (size_t v = 0; v < graph.Size(); ++v) {
                vector<size_t> vertex_color(graph.Size(), 0);
                size_t colors = 1;
                vector<wval> component_dists = {0};
                for (const Edge& e : graph.GetEdges(v)) {
                    if (v == e.to) {
                        continue;
                    }
                    if (vertex_color[e.to] > 0) {
                        RelaxMin(component_dists[vertex_color[e.to]], e.weight);
                        continue;
                    }

                    queue<size_t> q;
                    q.push(e.to);
                    vertex_color[e.to] = colors;
                    while (!q.empty()) {
                        const size_t u = q.front();
                        q.pop();
                        for (const Edge& f : graph.GetEdges(u)) {
                            if (f.to != v && vertex_color[f.to] == 0) {
                                q.push(f.to);
                                vertex_color[f.to] = colors;
                            }
                        }
                    }

                    component_dists.push_back(e.weight);
                    ++colors;
                }
                for (const wval dist : component_dists) {
                    RelaxMax((*lower_bounds)[v], dist);
                }
            }
        }

        vector<wval> CalcFarthestVertexUB(const Graph& graph) {
            size_t num_vertices = graph.Size();
            vector<wval> ubounds(num_vertices, wval(0));
            for (size_t v = 0; v < num_vertices; ++v) {
                for (const Edge& e : graph.GetEdges(v)) {
                    RelaxMax(ubounds[v], e.weight);
                }
            }
            return ubounds;
        }

        vector<wval> CalcPrimUB(const Graph& graph) {
            if (graph.Size() == 0) {
                return vector<wval>();
            }
            vector<bool> used(graph.Size(), false);
            vector<wval> ubounds(graph.Size(), wval(0));

            struct SpanningTreeExtender {
                wval weight;
                size_t from;
                size_t to;

                bool operator<(const SpanningTreeExtender& other) const {
                    return weight > other.weight;
                }
            };

            priority_queue<SpanningTreeExtender> q;
            q.emplace(SpanningTreeExtender{0, 0, 0});

            while (!q.empty()) {
                SpanningTreeExtender top = q.top();
                q.pop();
                if (used[top.to]) {
                    continue;
                }
                used[top.to] = true;
                RelaxMax(ubounds[top.from], top.weight);
                RelaxMax(ubounds[top.to], top.weight);
                for (const Edge& edge : graph.GetEdges(top.to)) {
                    if (!used[edge.to]) {
                        q.emplace(SpanningTreeExtender{edge.weight, top.to, edge.to});
                    }
                }
            }

            return ubounds;
        }

        template <class T>
        T Sum(const std::vector<T>& arr) {
            T sum = 0;
            for (const auto& val : arr) {
                sum += val;
            }
            return sum;
        }

        Graph RemoveHeavyEdges(const Graph& graph, const wval solution_upperbound) {
            Graph result(graph.Size());
            for (size_t v = 0; v < graph.Size(); ++v) {
                for (const Edge& edge : graph.GetEdges(v)) {
                    if (v < edge.to && LessEq(edge.weight, solution_upperbound)) {
                        result.AddEdge(v, edge.to, edge.weight);
                    }
                }
            }
            return result;
        }
    }

    LowerBoundsType GetLowerBoundsType(const std::string& name) {
        const auto iter = LowerBoundsTypeNames.find(name);
        if (iter == LowerBoundsTypeNames.end()) {
            throw std::invalid_argument("Unknown lower bounds type: " + name);
        }
        return iter->second;
    }

    std::vector<wval> CalcLowerBounds(const Graph& graph, const LowerBoundsType type) {
        std::vector<wval> lower_bounds(graph.Size(), wval(0));
        if (type == LowerBoundsType::None) {
            // do nothing
        } else if (type == LowerBoundsType::ClosestVertex) {
            CalcClosestVertexLB(graph, &lower_bounds);
        } else if (type == LowerBoundsType::ClosestVertexAndLeaves) {
            CalcClosestVertexLB(graph, &lower_bounds);
            CalcLeavesLB(graph, &lower_bounds);
        } else if (type == LowerBoundsType::AnySolution) {
            CalcAnySolutionLB(graph, &lower_bounds);
        }
        return lower_bounds;
    }

    Graph GetObligatoryGraph(const Graph& graph, const vector<wval>& lower_bounds) {
        size_t n = graph.Size();
        Graph og(n);
        for (size_t v = 0; v < n; ++v) {
            for (const Edge& e : graph.GetEdges(v)) {
                if (v < e.to && LessEq(e.weight, lower_bounds[v]) && LessEq(e.weight, lower_bounds[e.to])) {
                    og.AddEdge(v, e.to, e.weight);
                }
            }
        }
        return og;
    }

    int GetNumObligatoryComponents(const string& path, const LowerBoundsType lower_bounds_type) {
        return GetObligatoryComponents(path, lower_bounds_type).size();
    }

    int GetNumObligatoryComponents(const std::string& path, const std::string& lower_bounds_type) {
        return GetNumObligatoryComponents(path, GetLowerBoundsType(lower_bounds_type));
    }

    std::vector<std::vector<size_t>> GetObligatoryComponents(const std::string& path,
                                                             const LowerBoundsType lower_bounds_type) {
        const auto graph = ReadGraph(path);
        const auto lower_bounds = CalcLowerBounds(graph, lower_bounds_type);
        const Graph obligatory_graph = GetObligatoryGraph(graph, lower_bounds);
        const auto obligatory_components = obligatory_graph.FindComponents();
        return obligatory_components;
    }

    std::vector<std::vector<size_t>> GetObligatoryComponents(const std::string& path,
                                                             const std::string& lower_bounds_type) {
        return GetObligatoryComponents(path, GetLowerBoundsType(lower_bounds_type));
    }

    wval CalcUpperBound(const Graph& graph,
                        const UpperBoundType type,
                        const std::vector<wval>& lower_bounds) {
        std::vector<wval> upper_bounds;
        if (type == UpperBoundType::FarthestVertex) {
            upper_bounds = CalcFarthestVertexUB(graph);
        } else if (type == UpperBoundType::Prim) {
            upper_bounds = CalcPrimUB(graph);
        }
        if (!lower_bounds.empty()) {
            for (size_t vertex = 0; vertex < upper_bounds.size(); ++vertex) {
                RelaxMax(upper_bounds[vertex], lower_bounds[vertex]);
            }
        }
        return Sum(upper_bounds);
    }

    Graph BuildReducedGraph(const Graph& graph,
                            const wval solution_upper_bound,
                            const std::vector<wval>& lower_bounds,
                            const bool cut_edges_once) {
        const auto light_graph = RemoveHeavyEdges(graph, solution_upper_bound);

        if (cut_edges_once) {
            return light_graph;
        }

        const auto solution_lower_bound = Sum(lower_bounds);
        size_t n = light_graph.Size();
        Graph reduced_graph(n);
        for (size_t v = 0; v < n; ++v) {
            for (const Edge & e : light_graph.GetEdges(v)) {
                size_t u = e.to;
                const wval w = e.weight;
                const wval cost_increase = (w - lower_bounds[v]) + (w - lower_bounds[u]);
                if (v < u && LessEq(solution_lower_bound + cost_increase , solution_upper_bound)) {
                    reduced_graph.AddEdge(v, u, w);
                }
            }
        }

        return reduced_graph;
    }
}
