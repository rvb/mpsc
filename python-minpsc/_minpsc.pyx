import time

from libcpp cimport vector
from cython.operator cimport dereference

from _minpsc cimport (wval, DpSolver, CplexEx1Solver, CplexEx2Solver,
                      SimpleBruteSolver, SmartBruteSolver, GetNumObligatoryComponents,
                      GetObligatoryComponents)


DEFAULT_LOWER_BOUNDS = 'closest-vertex-and-leaves'.encode('utf8')
NO_LOWER_BOUNDS = 'none'.encode('utf8')


class ProgressAccounter:
    def __init__(self):
        self._transform_start = None
        self._solve_start = None
        self.result = {}

    def start_transform(self, num_edges, num_vertices):
        self.result['edges_before'] = num_edges
        self.result['vertices_before'] = num_vertices
        self._transform_start = time.time()

    def end_transform(self):
        self.result['transform_time'] = time.time() - self._transform_start

    def start_solve(self, num_edges, num_vertices):
        self.result['edges_after'] = num_edges
        self.result['vertices_after'] = num_vertices
        self._solve_start = time.time()

    def end_solve(self, cost):
        self.result['solve_time'] = time.time() - self._solve_start
        self.result['cost'] = cost

    def add_to_result(self, name, data):
        self.result[name] = data


def _dp(const string& graph_path, const bool optimize, const string& lower_bounds_type, eps, seed, **kwargs):
    cdef DpSolver* solver = new DpSolver(eps, seed, lower_bounds_type)
    dereference(solver).SetGraph(graph_path)
    acc = ProgressAccounter()
    acc.start_transform(dereference(solver).GetEdgesNum(), dereference(solver).GetVerticesNum())
    dereference(solver).Transform(optimize)
    acc.end_transform()
    acc.start_solve(dereference(solver).GetEdgesNum(), dereference(solver).GetVerticesNum())
    acc.end_solve(dereference(solver).Solve())
    acc.add_to_result('solution_upperbound', dereference(solver).GetSolutionUpperBound())
    acc.add_to_result('num_obligatory_components', dereference(solver).GetNumObligatoryComponents())
    acc.add_to_result('num_optimal_solutions_found', dereference(solver).GetNumOptimalSolutionsFound())
    acc.add_to_result('max_optimal_solutions_density', dereference(solver).GetMaxOptimalSolutionsDensity())
    del solver
    return acc.result


def _ex1(const string& graph_path, const string& lower_bounds_type, start_vertex=0, **kwargs):
    cdef CplexEx1Solver* solver = new CplexEx1Solver(start_vertex, lower_bounds_type)
    assert dereference(solver).IsAvailable(), "The package was built without CPLEX: the EX1 solver is not available."
    dereference(solver).SetGraph(graph_path)
    acc = ProgressAccounter()
    acc.start_transform(dereference(solver).GetEdgesNum(), dereference(solver).GetVerticesNum())
    dereference(solver).Transform(True)
    acc.end_transform()
    acc.start_solve(dereference(solver).GetEdgesNum(), dereference(solver).GetVerticesNum())
    acc.end_solve(dereference(solver).Solve())
    acc.add_to_result('solution_upperbound', dereference(solver).GetSolutionUpperBound())
    return acc.result


def _ex2(const string& graph_path, const string& lower_bounds_type, **kwargs):
    cdef CplexEx2Solver* solver = new CplexEx2Solver(lower_bounds_type)
    assert dereference(solver).IsAvailable(), "The package was built without CPLEX: the EX2 solver is not available."
    dereference(solver).SetGraph(graph_path)
    acc = ProgressAccounter()
    acc.start_transform(dereference(solver).GetEdgesNum(), dereference(solver).GetVerticesNum())
    dereference(solver).Transform(True)
    acc.end_transform()
    acc.start_solve(dereference(solver).GetEdgesNum(), dereference(solver).GetVerticesNum())
    acc.end_solve(dereference(solver).Solve())
    acc.add_to_result('solution_upperbound', dereference(solver).GetSolutionUpperBound())
    return acc.result


def _bf(const string& graph_path, const string& lower_bounds_type, **kwargs):
    cdef SmartBruteSolver* solver = new SmartBruteSolver(lower_bounds_type)
    dereference(solver).SetGraph(graph_path)
    acc = ProgressAccounter()
    acc.start_transform(dereference(solver).GetEdgesNum(), dereference(solver).GetVerticesNum())
    dereference(solver).Transform(True)
    acc.end_transform()
    acc.start_solve(dereference(solver).GetEdgesNum(), dereference(solver).GetVerticesNum())
    acc.end_solve(dereference(solver).Solve())
    return acc.result


def solve(solver_name, graph_path, **kwargs):
    graph_path = graph_path.encode()
    if solver_name == 'dp1':
        return _dp(graph_path, True, DEFAULT_LOWER_BOUNDS, **kwargs)
    elif solver_name == 'dp2':
        return _dp(graph_path, False, DEFAULT_LOWER_BOUNDS, **kwargs)
    elif solver_name == 'ex1':
        return _ex1(graph_path, NO_LOWER_BOUNDS, **kwargs)
    elif solver_name == 'ex2':
        return _ex2(graph_path, NO_LOWER_BOUNDS, **kwargs)
    elif solver_name == 'ex1lb':
        return _ex1(graph_path, DEFAULT_LOWER_BOUNDS, **kwargs)
    elif solver_name == 'ex2lb':
        return _ex2(graph_path, DEFAULT_LOWER_BOUNDS, **kwargs)
    elif solver_name == 'bf':
        return _bf(graph_path, DEFAULT_LOWER_BOUNDS, **kwargs)
    else:
        assert False, f"No such solver: {solver_name}"

def get_num_obligatory_components(path, lower_bounds_type=DEFAULT_LOWER_BOUNDS):
    path = path.encode('utf8')
    return GetNumObligatoryComponents(path, lower_bounds_type)

def get_obligatory_components(path, lower_bounds_type=DEFAULT_LOWER_BOUNDS):
    path = path.encode('utf8')
    return [[v for v in comp] for comp in GetObligatoryComponents(path, lower_bounds_type)]
