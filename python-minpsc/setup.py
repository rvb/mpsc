import os
from setuptools import find_packages, setup
from setuptools.extension import Extension
from Cython.Build import cythonize

extra_compile_args=['-std=c++17', '-O2']
include_dirs = []
library_dirs = []
libraries=['m', 'pthread', 'dl']

CPLEX_SYSTEM = os.getenv('CPLEX_SYSTEM')
CPLEX_STUDIO_PATH = os.getenv('CPLEX_STUDIO_PATH')
if CPLEX_SYSTEM is not None or CPLEX_STUDIO_PATH is not None:
    assert CPLEX_SYSTEM is not None, 'CPLEX_SYSTEM environment variable must be set'
    assert CPLEX_STUDIO_PATH is not None, 'CPLEX_STUDIO_PATH environment variable must be set'

    LIBFORMAT = "static_pic"
    CPLEXDIR = os.path.join(CPLEX_STUDIO_PATH, "cplex")
    CONCERTDIR = os.path.join(CPLEX_STUDIO_PATH, "concert")
    CPLEXLIBDIR = os.path.join(CPLEXDIR, "lib", CPLEX_SYSTEM, LIBFORMAT)
    CONCERTLIBDIR = os.path.join(CONCERTDIR, "lib", CPLEX_SYSTEM, LIBFORMAT)
    CPLEXINCDIR = os.path.join(CPLEXDIR, "include")
    CONCERTINCDIR = os.path.join(CONCERTDIR, "include")

    extra_compile_args += ['-DUSE_CPLEX']
    include_dirs += [CPLEXINCDIR, CONCERTINCDIR]
    library_dirs += [CPLEXLIBDIR, CONCERTLIBDIR]
    libraries += ['concert', 'ilocplex', 'cplex']

ext = Extension('_minpsc',
                ['_minpsc.pyx'],
                language='c++',
                extra_compile_args=extra_compile_args,
                include_dirs=include_dirs,
                library_dirs=library_dirs,
                libraries=libraries)
setup(name='minpsc',
      py_modules=['fastweihe'],
      packages=find_packages(),
      ext_modules=cythonize([ext], compiler_directives={'language_level': 3}))
