#!/bin/env python
import argparse
import os
import tempfile

import numpy as np

from generators import add_generator_arguments, get_generator_names, get_instance_generator
from helpers import eprint, given_random_seed
from minpsc import get_num_obligatory_components, solve


def fill_columns(results):
    columns = []
    for algo, stats in results.items():
        for stat in stats.keys():
            columns.append({
                'name': f"{algo}_{stat}",
                'algo': algo,
                'stat': stat
            })
    return columns

def add_first_line(path, columns, generator_keys):
    line = '\t'.join(['sample'] +
                     list(generator_keys) +
                     [col['name'] for col in columns])
    with open(path, 'w') as f:
        print(line, file=f)


def add_line(path, columns, results, generator_keys, generator_args, sample):
    line = '\t'.join([str(sample)] +
                     [str(generator_args[key]) for key in generator_keys] +
                     [str(results[col['algo']][col['stat']]) for col in columns])
    with open(path, 'a') as f:
        print(line, file=f)


def search_and_solve(output, algos, algo_kwargs, graph_output_dir, generator_name,
                     grid_sizes, generator_args, num_samples, seed):
    first_entry = True
    columns = []
    for grid_size in grid_sizes:
        if grid_size is not None:
            eprint(f"---------------")
            eprint(f"GRID SIZE = {grid_size}")
            eprint(f"---------------")
            generator_args['grid_size'] = grid_size
        for sample in range(num_samples):
            combined_seed = [sample, seed, generator_args['grid_size'] or 0]
            with given_random_seed(combined_seed):
                eprint('SEARCHING...')
                generator = get_instance_generator(generator_name, **generator_args)
                if grid_size is not None:
                    file_name = f"graph_seed_{seed}_sample_{sample}_greed_size_{grid_size}.graph"
                else:
                    file_name = f"graph_seed_{seed}_sample_{sample}.graph"
                graph_path = os.path.join(graph_output_dir, file_name)
                generator(graph_path)
                del generator

                eprint('SOLVING...')
                results = {}
                for algo in algos:
                    eprint(algo)
                    results[algo] = solve(algo, graph_path, **algo_kwargs)

                if output is None:
                    continue

                if first_entry:
                    first_entry = False
                    columns = fill_columns(results)
                    generator_keys = list(generator_args.keys())
                    add_first_line(output, columns, generator_keys)

                add_line(output, columns, results, generator_keys, generator_args, sample)
                eprint('------------')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate instances and run solvers on them.')

    general_parser = parser.add_argument_group('general')
    general_parser.add_argument('--generator', dest='generator', type=str, required=True,
                                choices=get_generator_names(), help='An instance generator type.')
    general_parser.add_argument('--seed', dest='seed', type=int, default=0, help='Common random seed for each sample.')
    general_parser.add_argument('--num-samples', dest='num_samples',  default=10, type=int,
                                help='The number of samples per fixed grid size.')
    general_parser.add_argument('--output', dest='output', type=str, default=None, help='A path to store the output table.')
    general_parser.add_argument('--dynamic-grid-size', dest='dynamic_grid_size', type=int, nargs='+', default=None,
                                help='Grid sizes to run through. If set, the grid_size generator parameter is ignored.')
    general_parser.add_argument('--store-dataset-path', dest='store_dataset_path', type=str, default=None,
                                help='If set, the generated graphs are stored in the specified directory.')

    algorithms_parser = parser.add_argument_group('algorithms')
    algorithms_parser.add_argument('--ex1', dest='ex1', action='store_true')
    algorithms_parser.add_argument('--ex2', dest='ex2', action='store_true')
    algorithms_parser.add_argument('--ex1lb', dest='ex1lb', action='store_true')
    algorithms_parser.add_argument('--ex2lb', dest='ex2lb', action='store_true')
    algorithms_parser.add_argument('--dp1', dest='dp1', action='store_true')
    algorithms_parser.add_argument('--dp2', dest='dp2', action='store_true')
    algorithms_parser.add_argument('--bf', dest='bf', action='store_true')

    algorithms_parser.add_argument('--start-vertex', dest='start_vertex', default=0, type=int, help='The starting vertex parameter for EX1.')
    algorithms_parser.add_argument('--eps', dest='eps', default=0.1, type=float, help='The error probability parameter for DP1 and DP2.')
    algorithms_parser.add_argument('--dp-seed', dest='dp_seed', default=0, type=int, help='The random seed parameter for DP1 and DP2.')

    generator_parser = parser.add_argument_group('generator_args')
    add_generator_arguments(generator_parser)

    args = parser.parse_args()

    assert args.grid_size is not None or args.dynamic_grid_size, 'grid size must be set'

    generator_args = {arg.dest: getattr(args, arg.dest, None)
                      for arg in generator_parser._group_actions}

    grid_sizes = args.dynamic_grid_size
    if grid_sizes is None:
        grid_sizes = [None]

    algos = []
    if args.ex1:
        algos.append('ex1')
    if args.ex2:
        algos.append('ex2')
    if args.ex1lb:
        algos.append('ex1lb')
    if args.ex2lb:
        algos.append('ex2lb')
    if args.dp1:
        algos.append('dp1')
    if args.dp2:
        algos.append('dp2')
    if args.bf:
        algos.append('bf')

    algo_kwargs = {
        'start_vertex': args.start_vertex,
        'eps': args.eps,
        'seed': args.dp_seed,
    }

    if args.store_dataset_path is None:
        with tempfile.TemporaryDirectory() as temp_dir:
            search_and_solve(args.output, algos, algo_kwargs, temp_dir, args.generator,
                             grid_sizes, generator_args, args.num_samples, args.seed)
    else:
        assert os.path.isdir(args.store_dataset_path), f"no such directory: {args.store_dataset_path}"
        search_and_solve(args.output, algos, algo_kwargs, args.store_dataset_path, args.generator,
                         grid_sizes, generator_args, args.num_samples, args.seed)
