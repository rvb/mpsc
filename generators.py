import abc
import itertools

import numpy as np

from helpers import eprint
from minpsc import get_obligatory_components


EPS = 1e-9


class InstanceGenerator(abc.ABC):
    def __init__(self, gamma=2, num_obligatory_components=2, **kwargs):
        self._gamma = gamma
        self._num_obligatory_components = num_obligatory_components

    def __call__(self, path):
        while True:
            num_vertices, edges, points = self._generate()
            self._write_graph(path, num_vertices, edges)
            del edges

            obligatory_components = get_obligatory_components(path)
            num_obligatory_components = len(obligatory_components)
            eprint(f"{num_obligatory_components} obligatory components")

            if (self.num_obligatory_components is None or
                num_obligatory_components == self.num_obligatory_components):

                return points, obligatory_components

            del points
            del obligatory_components

    @abc.abstractmethod
    def _generate(self):
        raise NotImplemented

    def _write_graph(self, path, num_vertices, edges):
        with open(path, 'w') as f:
            num_edges = len(edges)
            f.write(f"{num_vertices} {num_edges}\n")
            for edge in edges:
                f.write(' '.join(map(str, edge)) + '\n')

    def get_weight(self, first_point, second_point):
        dx = first_point[0] - second_point[0]
        dy = first_point[1] - second_point[1]
        sq_sum = dx * dx + dy * dy
        if self._gamma == 2:
            weight = sq_sum
        else:
            weight = sq_sum ** (self._gamma / 2)
        return int(np.ceil(weight - EPS))

    @abc.abstractmethod
    def get_image(self, points, components):
        raise NotImplemented

    @property
    def num_obligatory_components(self):
        return self._num_obligatory_components


class FloatingInstanceGenerator(InstanceGenerator):
    def __init__(self, image_size=100, **kwargs):
        super(FloatingInstanceGenerator, self).__init__(**kwargs)

        self._image_size = image_size

    def _calc_borders(self, points):
        xs, ys = map(np.array, zip(*points))

        return xs.min() - EPS, xs.max() + EPS, ys.min() - EPS, ys.max() + EPS

    def get_image(self, points, components):
        grid = np.zeros(shape=(self._image_size, self._image_size), dtype=int)

        xmin, xmax, ymin, ymax = self._calc_borders(points)

        for color, comp in enumerate(components, start=1):
            for vertex in comp:
                x, y = points[vertex]
                i = int((x - xmin) / (xmax - xmin) * self._image_size)
                j = int((y - ymin) / (ymax - ymin) * self._image_size)
                grid[i, j] = color

        return grid


class FloatingGridInstanceGenerator(FloatingInstanceGenerator):
    def __init__(self, grid_size, triangular_grid, **kwargs):
        super(FloatingGridInstanceGenerator, self).__init__(**kwargs)

        assert isinstance(grid_size, int), 'an integer grid_size must be set'
        self._grid_size = grid_size
        self._triangular_grid = triangular_grid

    @property
    def grid_size(self):
        return self._grid_size

    @property
    def triangular_grid(self):
        return self._triangular_grid

    def _generate(self):
        points = self.generate_points()
        num_vertices, edges = self._build_graph(points)
        return num_vertices, edges, points

    def generate_points(self):
        points = self._generate_points()
        points = self._transform(points)
        return points

    def _generate_points(self):
        if self.triangular_grid:
            return self._generate_triangular()
        return self._generate_square()

    def _generate_triangular(self):
        points = []
        for row, y in enumerate(np.arange(0, self.grid_size + EPS, np.sqrt(3) * 0.5)):
            for x in np.arange(0.5 * (row % 2), self.grid_size + EPS, 1):
                points.append((x, y))
        return points

    def _generate_square(self):
        points = []
        for i in range(self.grid_size):
            for j in range(self.grid_size):
                points.append((i, j))
        return points

    def _build_graph(self, points):
        edges = []
        for i in range(len(points)):
            for j in range(i + 1, len(points)):
                edges.append((i, j, self.get_weight(points[i], points[j])))
        num_vertices = len(points)
        return num_vertices, edges

    @abc.abstractmethod
    def _transform(self, points):
        return points


class SimpleFloatingGridInstanceGenerator(FloatingGridInstanceGenerator):
    def __init__(self, failed_sensors_fraction, **kwargs):
        super(SimpleFloatingGridInstanceGenerator, self).__init__(**kwargs)

        if failed_sensors_fraction is None:
            failed_sensors_fraction = self._auto_failed_sensors_fraction(self.grid_size,
                                                                         self.triangular_grid)
        self._left_sensors_fraction = 1.0 - failed_sensors_fraction
        assert self._left_sensors_fraction > 0.0, \
               'the number of failed sensors must be less than the number of sensors'

    def _auto_failed_sensors_fraction(self, grid_size, triangular_grid):
        return 0.1 + 8 / (3 * grid_size) if not self.triangular_grid else 0.1 + 1 / np.sqrt(grid_size)

    def _transform(self, points):
        points_left = int(len(points) * self._left_sensors_fraction)
        return [points[i] for i in np.random.permutation(len(points))[:points_left]]


class PerlinNoiseFloatingGridInstanceGenerator(FloatingGridInstanceGenerator):
    def __init__(self, perlin_grid_size, noise_threshold, perlin_free_corners, **kwargs):
        super(PerlinNoiseFloatingGridInstanceGenerator, self).__init__(**kwargs)

        self._perlin_grid_size = perlin_grid_size
        assert self._perlin_grid_size > 1, 'Perlin noise grid size must be at least 2'
        self._perlin_free_corners = perlin_free_corners
        self._noise_threshold = noise_threshold

    def _transform(self, points):
        angles = np.random.rand(self._perlin_grid_size, self._perlin_grid_size) * (2.0 * np.pi)
        if not self._perlin_free_corners:
            angles = self._fix_corners(angles)
        self._grads = np.stack([np.cos(angles), np.sin(angles)], axis=2)
        if self.triangular_grid:
            scale = (self._perlin_grid_size - 1) / self.grid_size
        else:
            scale = (self._perlin_grid_size - 1) / (self.grid_size - 1)
        return [point for point in points if self._noise(point, scale, self._grads) < self._noise_threshold - EPS]

    def grayscale(self, size=1200):
        grayscale = np.zeros((size, size), dtype=float)
        for x in range(size):
            for y in range(size):
                grayscale[x, y] = self._noise((x, y), (self._perlin_grid_size - 1) / (size - 1), self._grads)
        return grayscale

    @property
    def grads(self):
        return self._grads

    def _noise(self, point, scale, grads):
        x, y = point
        p = np.array([x, y])
        scaled_p = p * scale

        i1 = min(int(scaled_p[0]), self._perlin_grid_size - 2)
        j1 = min(int(scaled_p[1]), self._perlin_grid_size - 2)
        i2 = i1 + 1
        j2 = j1 + 1

        corner_indices = list(itertools.product([i1, i2], [j1, j2]))
        corner_is, corner_js = zip(*corner_indices)

        corner_grads = grads[corner_is, corner_js]
        corner_offsets = scaled_p - np.array(corner_indices)
        scalar_prod = np.sum(corner_grads * corner_offsets, axis=1)

        return np.sum(scalar_prod * np.prod(1.0 - self._smoothstep(np.abs(corner_offsets)), axis=1))

    def _fix_corners(self, angles):
        M = self._perlin_grid_size - 1

        angles[:, 0] = 1 / 2 * np.pi
        angles[:, M] = -1 / 2 * np.pi
        angles[0, :] = 0
        angles[M, :] = np.pi

        angles[0, 0] = 1 / 4 * np.pi
        angles[0, M] = -1 / 4 * np.pi
        angles[M, 0] = 3 / 4 * np.pi
        angles[M, M] = -3 / 4 * np.pi

        return angles

    def _smoothstep(self, x):
        return (3 - 2 * x) * x * x


class LakesInstanceGenerator(FloatingInstanceGenerator):
    def __init__(self, num_lakes, grid_size, perlin_grid_size, lake_shift_factor, triangular_grid, **kwargs):
        super(LakesInstanceGenerator, self).__init__(**kwargs)

        self._num_lakes = num_lakes
        assert self._num_lakes is not None and self._num_lakes > 0, 'num_lakes parameter must be a positive integer'
        self._noise_generator = PerlinNoiseFloatingGridInstanceGenerator(grid_size=grid_size,
                                                                         perlin_grid_size=perlin_grid_size,
                                                                         triangular_grid=triangular_grid,
                                                                         noise_threshold=0.0,
                                                                         perlin_free_corners=False)
        if lake_shift_factor is None:
            lake_shift_factor = self.num_lakes or 1.0
        self._lake_shift_factor = lake_shift_factor


    def _generate(self):
        num_vertices = 0
        edges = []
        points = []
        for lake_id in range(self._num_lakes):
            lake = self._generate_lake()

            for i in range(len(lake)):
                for j in range(i + 1, len(lake)):
                    edges.append((i + num_vertices, j + num_vertices, self.get_weight(lake[i], lake[j])))

            lake = self._center_lake(lake)
            lake = self._rotate_lake(lake, np.random.rand() * (2.0 * np.pi))
            lake = self._shift_lake(lake)
            lake = self._rotate_lake(lake, (2.0 * np.pi) * (lake_id / self._num_lakes))

            for i in range(num_vertices):
                for j in range(len(lake)):
                    edges.append((i, j + num_vertices, self.get_weight(points[i], lake[j])))

            num_vertices += len(lake)
            points += lake

        return num_vertices, edges, points

    def _generate_lake(self):
        points = self._noise_generator.generate_points()

        num_vertices = len(points)
        edges = [[] for vertex in range(num_vertices)]
        for i in range(len(points)):
            for j in range(i):
                if self.get_weight(points[i], points[j]) == 1:
                    edges[i].append(j)
                    edges[j].append(i)

        components = self._find_connected_components(edges, num_vertices)
        max_component_index = np.argmax(np.array(list(map(len, components))))

        new_points = []
        for i in components[max_component_index]:
            new_points.append(points[i])
        return new_points

    def _center_lake(self, lake):
        xs, ys = map(np.array, zip(*lake))
        x_bias = (xs.max() + xs.min()) / 2
        y_bias = (ys.max() + ys.min()) / 2
        new_lake = []
        for x, y in lake:
            x -= x_bias
            y -= y_bias
            new_lake.append((x, y))
        return new_lake

    def _rotate_lake(self, lake, angle):
        sina = -np.sin(angle)
        cosa = np.cos(angle)
        new_lake = []
        for x, y in lake:
            x, y = x * cosa - y * sina, x * sina + y * cosa
            new_lake.append((x, y))
        return new_lake

    def _shift_lake(self, lake):
        new_lake = []
        for x, y in lake:
            x += (self._noise_generator.grid_size - 1) * self._lake_shift_factor
            new_lake.append((x, y))
        return new_lake

    def _find_connected_components(self, edges, num_vertices):
        used = set()
        components = []
        for v in range(num_vertices):
            if v in used:
                continue
            components.append(self._find_maximal_component(edges, v, used))
        return components

    def _find_maximal_component(self, edges, v, used):
        component = []
        queue = [v]
        start = 0

        while start < len(queue):
            v = queue[start]
            start += 1
            component.append(v)
            used.add(v)

            for u in edges[v]:
                if u not in used:
                    queue.append(u)
                    used.add(u)

        return component


_GENERATOR_TYPES = {
    'simple': SimpleFloatingGridInstanceGenerator,
    'perlin-noise': PerlinNoiseFloatingGridInstanceGenerator,
    'lakes': LakesInstanceGenerator,
}


def get_generator_names():
    return _GENERATOR_TYPES.keys()


def get_instance_generator(name, **kwargs):
    generator_class = _GENERATOR_TYPES.get(name, None)
    assert generator_class is not None, 'No generator for type: ' + name
    return generator_class(**kwargs)


def add_generator_arguments(parser):
    parser.add_argument('--grid-size', dest='grid_size', type=int, help="Size of the grid. (Use for 'simple', 'perlin-noise', 'lakes'.)")
    parser.add_argument('--failed-sensors-fraction', dest='failed_sensors_fraction', type=float,
                        default=None, help="The fraction of failed sensors. (Use for 'simple'.")
    parser.add_argument('--noise-threshold', dest='noise_threshold', type=float, default=0.0,
                        help="Noise threshold. (Use for 'perlin-noise' and 'lakes'.)")
    parser.add_argument('--perlin-grid-size', dest='perlin_grid_size', type=int, default=7,
                        help="The gradient grid size for Perlin noise. (Use for 'perlin-noise' and 'lakes'.)")
    parser.add_argument('--perlin-free-corners', dest='perlin_free_corners', action='store_true',
                        help="Whether all lakes don\'t have to be inside the grid. (Use for 'perlin-noise'.)")
    parser.add_argument('--num-lakes', dest='num_lakes', type=int, default=None,
                        help="The number of lakes, defaults to the num-obligatory-components parameters if it is set. (Use for 'lakes'.)")
    parser.add_argument('--lake-shift-factor', dest='lake_shift_factor', type=float, default=1.0,
                        help="Distance to shift each lake from the grid center relative to the grid size. (Use for 'lakes'.)")
    parser.add_argument('--num-obligatory-components', dest='num_obligatory_components', type=int, default=None,
                        help="The number of obligatory components. (Use for 'simple', 'perlin-noise', 'lakes'.)")
    parser.add_argument('--triangular-grid', dest='triangular_grid', action='store_true',
                        help="Whether to layout sensors on a triangular grid. (Use for 'simple', 'perlin-noise', 'lakes'.)")
