#!/usr/bin/bash
cd "$(dirname ${BASH_SOURCE[0]})"
if [ ! -d "results" ]; then
    mkdir results
fi

# Perlin dp1
./run.py --generator perlin-noise --perlin-grid-size 7 --dynamic-grid-size 10 12 14 16 18 20 22 24 26 28 30 --triangular-grid --num-obligatory-components 3 --output results/perlin_C_3_dp1.txt --dp1
./run.py --generator perlin-noise --perlin-grid-size 7 --dynamic-grid-size 10 12 14 16 18 20 22 24 26 28 30 --triangular-grid --num-obligatory-components 4 --output results/perlin_C_4_dp1.txt --dp1
./run.py --generator perlin-noise --perlin-grid-size 7 --dynamic-grid-size 10 12 14 16 18 20 22 24 26 28 30 --triangular-grid --num-obligatory-components 5 --output results/perlin_C_5_dp1.txt --dp1
# Perlin ex2lb
./run.py --generator perlin-noise --perlin-grid-size 7 --dynamic-grid-size 10 12 14 16 18 20 22 24 26 28 30 --triangular-grid --num-obligatory-components 3 --output results/perlin_C_3_ex2lb.txt --ex2lb
./run.py --generator perlin-noise --perlin-grid-size 7 --dynamic-grid-size 10 12 14 16 18 20 22 24 26 28 30 --triangular-grid --num-obligatory-components 4 --output results/perlin_C_4_ex2lb.txt --ex2lb
./run.py --generator perlin-noise --perlin-grid-size 7 --dynamic-grid-size 10 12 14 16 18 20 22 24 26 28 30 --triangular-grid --num-obligatory-components 5 --output results/perlin_C_5_ex2lb.txt --ex2lb

# Simple dp1
./run.py --generator simple --dynamic-grid-size 10 20 30 40 50 60 70 80 --triangular-grid --num-obligatory-components 3 --output results/simple_C_3_dp1.txt --dp1
./run.py --generator simple --dynamic-grid-size 10 20 30 40 50 60 70 80 --triangular-grid --num-obligatory-components 4 --output results/simple_C_4_dp1.txt --dp1
./run.py --generator simple --dynamic-grid-size 10 20 30 40 50 60 70 80 --triangular-grid --num-obligatory-components 5 --output results/simple_C_5_dp1.txt --dp1
# Simple dp2
./run.py --generator simple --dynamic-grid-size 10 20 30 40 50 60 70 80 --triangular-grid --num-obligatory-components 3 --output results/simple_C_3_dp2.txt --dp2
./run.py --generator simple --dynamic-grid-size 10 20 30 40 50 60 70 80 --triangular-grid --num-obligatory-components 4 --output results/simple_C_4_dp2.txt --dp2
./run.py --generator simple --dynamic-grid-size 10 20 30 40 50          --triangular-grid --num-obligatory-components 5 --output results/simple_C_5_dp2.txt --dp2
# Simple ex2lb
./run.py --generator simple --dynamic-grid-size 10 20 30 40 50 60 70 80 --triangular-grid --num-obligatory-components 3 --output results/simple_C_3_ex2lb.txt --ex2lb
./run.py --generator simple --dynamic-grid-size 10 20 30 40 50 60 70 80 --triangular-grid --num-obligatory-components 4 --output results/simple_C_4_ex2lb.txt --ex2lb
./run.py --generator simple --dynamic-grid-size 10 20 30 40 50 60 70 80 --triangular-grid --num-obligatory-components 5 --output results/simple_C_5_ex2lb.txt --ex2lb
# Simple ex1lb
./run.py --generator simple --dynamic-grid-size 10 20 30 40 50 60 70 80 --triangular-grid --num-obligatory-components 3 --output results/simple_C_3_ex1lb.txt --ex1lb
./run.py --generator simple --dynamic-grid-size 10 20 30 40 50 60 70 80 --triangular-grid --num-obligatory-components 4 --output results/simple_C_4_ex1lb.txt --ex1lb
./run.py --generator simple --dynamic-grid-size 10 20 30 40 50 60 70    --triangular-grid --num-obligatory-components 5 --output results/simple_C_5_ex1lb.txt --ex1lb
# Simple bf
./run.py --generator simple --dynamic-grid-size 10 20 30 40 50 60 70 80 --triangular-grid --num-obligatory-components 3 --output results/simple_C_3_bf.txt --bf
./run.py --generator simple --dynamic-grid-size 10 20 30                --triangular-grid --num-obligatory-components 4 --output results/simple_C_4_bf.txt --bf
