#!/bin/env python
import argparse
import os
import shutil
import tempfile

import numpy as np
from PIL import Image, ImageDraw

from helpers import given_random_seed
from generators import add_generator_arguments, get_instance_generator, get_generator_names


def _generate_colors(num, seed):
    if num <= 7:
        return [[0, 255, 255],
                [255, 0, 255],
                [255, 255, 0],
                [0, 255, 0],
                [255, 0, 0],
                [0, 0, 255],
                [255, 255, 255]]
    with given_random_seed(seed):
        return [np.random.randint(0, 256, size=3) for i in range(num)]


def generate_colors(num, seed=0):
    return np.array(_generate_colors(num, seed))[:num]


def output_points(path, points, obligatory_components):
    component_ids = ['a'] * len(points)
    for component_id, component in enumerate(obligatory_components):
        for element in component:
            component_ids[element] = chr(ord('a') + component_id)
    with open(path, 'w') as f:
        f.write('x\ty\tc\n')
        f.write('\n'.join((f"{p[0]}\t{p[1]}\t{c}" for p, c in zip(points, component_ids))))


def output_image(path, points, obligatory_components):
    num_obligatory_components = len(obligatory_components)
    colors = generate_colors(num_obligatory_components)

    compressed_colors = generator.get_image(points, obligatory_components)
    width, height = compressed_colors.shape
    image_data = np.zeros((width, height, 3), dtype=np.uint8)
    for i in range(width):
        for j in range(height):
            color_id = compressed_colors[i, j]
            if color_id == 0:
                continue
            image_data[i, j, :] = colors[color_id - 1]

    image = Image.fromarray(image_data, 'RGB')
    image.save(path)


def output_grayscale(grayscale_path, lakes_path, point, obligatory_components):
    size = 1200
    gray = generator.grayscale(size)

    if grayscale_path is not None:
        scaled = ((gray - gray.min()) / (gray.max() - gray.min())) * 255
        gray_image = Image.fromarray(scaled).convert('RGB')

        def draw_arrow(image, x1, y1, grad, size):
            color = 0
            length = size / 20
            draw = ImageDraw.Draw(image)
            x1d5 = x1 + grad[0] * length * 0.75
            y1d5 = y1 + grad[1] * length * 0.75
            x2 = x1 + grad[0] * length
            y2 = y1 + grad[1] * length
            width = 5
            rad = 10
            draw.ellipse((y1 - rad, x1 - rad, y1 + rad, x1 + rad), fill=color)
            draw.line((y1, x1, y1d5, x1d5), fill=color, width=width)
            alpha = np.arctan2(grad[1], grad[0])
            x3 = x2 + np.cos(alpha + 10 / 12 * np.pi) * length / 2
            y3 = y2 + np.sin(alpha + 10 / 12 * np.pi) * length / 2
            x4 = x2 + np.cos(alpha - 10 / 12 * np.pi) * length / 2
            y4 = y2 + np.sin(alpha - 10 / 12 * np.pi) * length / 2
            draw.polygon((y2, x2, y3, x3, y4, x4), fill=color)

        grads = generator.grads
        N = grads.shape[0]
        for i in range(N):
            for j in range(N):
                draw_arrow(gray_image,
                           i / (N - 1) * (size - 1),
                           j / (N - 1) * (size - 1),
                           grads[i, j],
                           size)

        gray_image.save(grayscale_path)

    if lakes_path is not None:
        thresholded = np.zeros_like(gray) + 0.0 + 255.0 * (gray > 0.0 - 1e-9)
        thresh_image = Image.fromarray(thresholded).convert('RGB')
        thresh_image.save(lakes_path)


def visualize(args, generator, points, obligatory_components):
    if args.output_points is not None:
        output_points(args.output_points, points, obligatory_components)
    if args.output_image is not None:
        output_image(args.output_image, points, obligatory_components)
    if hasattr(generator, 'grayscale') and (args.output_grayscale is not None or args.output_lakes is not None):
        output_grayscale(args.output_grayscale, args.output_lakes, points, obligatory_components)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate an instance and output an image representation of it.')

    general_parser = parser.add_argument_group('general')
    general_parser.add_argument('--generator', dest='generator', type=str, required=True,
                                choices=get_generator_names(), help='A generator type.')
    general_parser.add_argument('--seed', dest='seed', type=int, default=0, help='A random seed.')
    general_parser.add_argument('--output-image', dest='output_image', type=str, default=None, help='An output points image .png path.')
    general_parser.add_argument('--output-points', dest='output_points', type=str, default=None, help='An output points .txt path.')
    general_parser.add_argument('--output-grayscale', dest='output_grayscale', type=str, default=None, help="An output grayscale .png path ('perlin-noise' generator only).")
    general_parser.add_argument('--output-lakes', dest='output_lakes', type=str, default=None, help="An output lakes .png path ('perlin-noise' generator only).")
    general_parser.add_argument('--output-graph', dest='output_graph', type=str, default=None, help="An output graph .graph path.")

    generator_parser = parser.add_argument_group('generator_args')
    add_generator_arguments(generator_parser)

    args = parser.parse_args()

    generator_args = {arg.dest: getattr(args, arg.dest, None)
                      for arg in generator_parser._group_actions}

    with tempfile.TemporaryDirectory() as temp_dir:
        generator_name = args.generator
        generator = get_instance_generator(generator_name, **generator_args)

        graph_path = os.path.join(temp_dir, 'graph.graph')

        with given_random_seed(args.seed):
            points, obligatory_components = generator(graph_path)

        if args.output_graph is not None:
            shutil.copyfile(graph_path, args.output_graph)

        visualize(args, generator, points, obligatory_components)
