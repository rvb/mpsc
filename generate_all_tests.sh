#!/usr/bin/bash
cd "$(dirname ${BASH_SOURCE[0]})"

# Perlin
mkdir -p data/perlin_C_3
./run.py --generator perlin-noise --perlin-grid-size 7 --dynamic-grid-size 10 12 14 16 18 20 22 24 26 28 30 --triangular-grid --num-obligatory-components 3 --store-dataset-path data/perlin_C_3
mkdir -p data/perlin_C_4
./run.py --generator perlin-noise --perlin-grid-size 7 --dynamic-grid-size 10 12 14 16 18 20 22 24 26 28 30 --triangular-grid --num-obligatory-components 4 --store-dataset-path data/perlin_C_4
mkdir -p data/perlin_C_5
./run.py --generator perlin-noise --perlin-grid-size 7 --dynamic-grid-size 10 12 14 16 18 20 22 24 26 28 30 --triangular-grid --num-obligatory-components 5 --store-dataset-path data/perlin_C_5

# Simple
mkdir -p data/simple_C_3
./run.py --generator simple --dynamic-grid-size 10 20 30 40 50 60 70 80 --triangular-grid --num-obligatory-components 3 --store-dataset-path data/simple_C_3
mkdir -p data/simple_C_4
./run.py --generator simple --dynamic-grid-size 10 20 30 40 50 60 70 80 --triangular-grid --num-obligatory-components 4 --store-dataset-path data/simple_C_4
mkdir -p data/simple_C_5
./run.py --generator simple --dynamic-grid-size 10 20 30 40 50 60 70 80 --triangular-grid --num-obligatory-components 5 --store-dataset-path data/simple_C_5
