from contextlib import contextmanager
import sys

import numpy as np


@contextmanager
def given_random_seed(seed):
    next_seed = np.random.randint(1 << 32)
    np.random.seed(seed)
    try:
        yield None
    finally:
        np.random.seed(next_seed)


def eprint(*args, **kwargs):
    print(*args, **kwargs, file=sys.stderr)
