Fixed-Parameter Algorithm for Min-Power Symmetric Connectivity (MPSC)
=====================================================================

The MPSC problem searches for optimal transmission power assignments
to wireless network nodes in order to establish a connected wireless
network. Stated formally:

- **Input:** A graph G=(V,E) with edge weights w:E→N.

- **Output:** Power assignments p:V→N with minimum sum so that the subgraph with the vertices V and edges { {u, v} ∈ E | w({u,v} ≤ min{p(u), p(v)} } is connected.

The problem is NP hard.  This tool computes and optimal solution cost
with a running time that depends single-exponentially on the number C
of connected components of an obligatory subgraph of the input
graph G, which consists of edges that can be assumed to be part
of the sought solution.  If C is logarithmic, the algorithm
thus runs in polynomial time. The algorithm is presented in

- [Matthias Bentert, René van Bevern, André Nichterlein, Rolf Niedermeier,
  Pavel V. Smirnov (2019).  Parameterized algorithms for power-efficiently
  connecting wireless sensor networks: Theory and experiments.](https://arxiv.org/abs/1706.03177)

On test instances with few connected components, the algorithm
outperforms state-of-the art integer linear programming models
presented in

- [R. Montemanni and L.M. Gambardella. 2005. Exact algorithms for the minimum power symmetric connectivity problem
in wireless networks. _Computers & Operations Research_ 32(11): 2891–2904.]( https://doi.org/10.1016/j.cor.2004.04.017)

Requirements
-----

The following is required:

* `Python 3.7+` with `pip` package manager
* `CPLEX` installed

The code was run on `Ubuntu 19.10`.
Compatibility with other operating systems was not checked.

Installation
-----

The testing environment uses a python package provided in this repository for test generation and solvers running.

### With ILP solvers

In order to use the ILP solvers, one must have CPLEX installed.
Install the python package with algorithms implementations with this line:
```
CPLEX_STUDIO_PATH=... CPLEX_SYSTEM=... pip install python-minpsc/
```

with `CPLEX_STUDIO_PATH` set to the path to `CPLEX` installation and `CPLEX_SYSTEM` set to the platform name used by `CPLEX`, e.g.:
```
CPLEX_STUDIO_PATH=/opt/ibm/ILOG/CPLEX_Studio1210 CPLEX_SYSTEM=x86-64_linux pip install python-minpsc/
```

### Without ILP solvers

If you don't have CPLEX installed or don't need the ILP solvers,
simply install the package with:
```
pip install python-minpsc/
```

Running tests
-----

You can perform an algorithm comparison with `run.py` script like this:
```
./run.py --generator perlin-noise --grid-size 20 --num-obligatory-components 4 --output comparison.txt --dp1 --ex2lb
```

Execute `./run.py --help` to get a full description of the script capabilities.

*Note: `simple` generator type corresponds to the ''faulty grid'' data set from the paper, and `perlin-noise` --- to the ''lakes'' data set.*

Run all experiments from the paper with:
```
./run_all_tests.sh
```

The output will appear in the `results` directory.

Generating test instances
-----

In order to generate tests, just execute `run.py` with `--store-dataset-path` (look at the "Running tests" section).
It will store all instances generated with the script in a specified directory.
Note, that you can omit the solving algorithm arguments if you only need the data.
Example:
```
./run.py --generator perlin-noise --grid-size 20 --num-obligatory-components 4 --store-dataset-path my_data_directory
```

The specified directory will contain the graph files.
Each one is a text file in the following format:
```
<N> <M>
<u1> <v1> <w1>
<u2> <v2> <w2>
...
<uM> <vM> <wM>
```

Here, `<N>` and `<M>` are the number of vertices and edges respectively.
The i-th edge has weight `<wi>` and vertices `<ui>` and `<vi>` as its endpoints.
The vertex indexation starts from 0.

Generate all tests from the paper with:
```
./generate_all_tests.sh
```

The output will appear in the `data` directory.
The whole data set will need about 15GB of space.

Visualizing test instances
-----

You can view what kind of instances are generated for any set of parameters with `visualize_instance.py` script.
Example:
```
./visualize_instance.py --generator perlin-noise --grid-size 20 --num-obligatory-components 4 --output-image out.png
```

Run `./visualize_instance.py --help` for full script capabilities description.

Acknowledgments
---------------

This tool was created by Pavel V. Smirnov, Novosibirsk State University,
supported by project 18-501-12031 NNIO_a,
"Trade-offs in parameterized data reduction",
of the [Russian Foundation for Basic Research](http://www.rfbr.ru).

----

René van Bevern <rvb@nsu.ru>
